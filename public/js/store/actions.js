import { autoScroll } from "../lib/autoScroll.js";
import { postTeiDoc, xmlTokensToHtml } from "../backend.js"
import {
    appendErrorSpan,
    createRevision,
    getSelectableTokens,
    getTokenInfo,
    isValidXTypeInput,
    removeErrorSpan,
    removeRevision,
    replaceElements,
    updateErrorSpan,
} from "../teiUtils.js"
import store from "./index.js";

/**
 * Returns the state machine state corresponding to the given token.
 * @param {*} tokenType - The token's type (word, punctuation or revision).
 * @returns {String}
 */
const getDisplayState = (tokenType) => {
    switch(tokenType) {
        case "word":
            return "displayingWord";
        case "punctuation":
            return "displayingPunctuation";
        case "revision":
            return "displayingRevision";
        default:
            return "";
    }
}

export default {
    appendError(context, {xType, reason, depId, eId, idX}) {
        if(!isValidXTypeInput(xType, context.data.autocomplete.xTypeCodes)) {
            context.data.errorInputFieldValues = { xType, reason, depId, eId, idX };
            context.data.validationHints.errorSubmission =
              "Please choose an xType code from the list of suggestions."; 
           
            return;
        }

        appendErrorSpan({xType, reason, depId, eId, idX}, context.data.selectedToken.domElement);

        context.data.selectedToken.info = getTokenInfo(context.data.selectedToken.domElement);
        context.data.validationHints.errorSubmission = "";
        context.data.errorInputFieldValues = {xType: "", reason: "", depId: "", eId: "", idX: ""};

        postTeiDoc({
            header: context.data.teiDoc.header,
            body: document.querySelector(".tei-container").innerHTML}
        );
    },
    appendErrorRequest(context, payload) {
        context.machineState = "appendingError";
    },
    async asyncFetchErrorCodes(context, payload) {
        try {
            /*
            TODO:
            Deal with the situation where the file does not exist or is corrupt.
            */
            const response = await fetch("/data/errorCodes.json");
            const data = await response.json();
            context.data.autocomplete.xTypeCodes = data;
        } catch (e) {
            console.log("Couldn't fetch the error codes!");
        }
    },
    async asyncUpdateCorrectedText(context, payload) {
        try {
            const data = await xmlTokensToHtml(payload);

            if (!data.error) {
                context.data.validationHints.correctedText = "";

                const correctedSpan = context.data.selectedToken.domElement.querySelector(".tei-corrected");
                correctedSpan.innerHTML = data.html;

                this.loadToken(context, context.data.selectedToken.domElement);

                postTeiDoc({
                        header: context.data.teiDoc.header,
                        body: document.querySelector(".tei-container").innerHTML}
                    );
            } else {
                context.data.validationHints.correctedText = data.error;
            }
        } catch(e) {
            console.log(e);
        }
        /*
        TODO:
        There's a bug that interferes with ".tei-space-character" span insertion between
        punctuation marks and words. Try correcting the text "Egils, þar" in
        verslo-JA0-t3.xml
        */
    },
    cancelAndGoBackToDisplayingToken(context, payload) {
        context.data.errorInputFieldValues = {xType: "", reason: "", depId: "", eId: "", idX: ""};
        context.data.validationHints.errorSubmission = "";

        /*
        TODO:
        Implement moving the cursor back to the first item in the selection. To
        do this, move the cursor position from the private variable in
        tokenCursor.js to a global variable in the store's data.js. This way the
        cancelAndGoBackToDisplayingToken() action can affect the cursor's
        position before it gets rendered by TokenCursor.render().
        */
        // if (context.machineState === "selectingMultipleTokens") {
        //     const firstTokenInSelection = document.querySelector(".selected-token");
        //     this.loadToken(context, firstTokenInSelection);
        //     console.log(context.data.selectedToken.domElement);
        // } else {
            switch(context.data.selectedToken.info.type) {
                case "word":
                    context.machineState = "displayingWord";
                    break;
                case "punctuation":
                    context.machineState = "displayingPunctuation";
                    break;
                case "revision":
                    context.machineState = "displayingRevision";
                    break;
                default:
                    break;
            }
        // }
    },
    confirmTokenSelection(context, payload) {
        const selectedTokens = document.querySelectorAll(".selected-token");
        const revision = createRevision("x", selectedTokens);
        replaceElements(selectedTokens, revision);
        context.data.cursorPosition = context.data.tokenSelection.start;

        postTeiDoc({
            header: context.data.teiDoc.header,
            body: document.querySelector(".tei-container").innerHTML}
        );

        this.loadToken(context, revision);
    },
    loadTeiHeader(context, payload) {
        context.data.teiDoc.header = payload;
        console.log(context.data.teiDoc);
    },
    loadToken(context, payload) {
        context.data.selectedToken.domElement = payload;
        context.data.selectedToken.info = getTokenInfo(payload); 

        context.machineState = getDisplayState(context.data.selectedToken.info.type);
    },
    moveCursorLeft(context, payload) {
        const tokens = document.querySelectorAll(".selectable-token");

        // Alias for readability
        let position = context.data.cursorPosition;

        position -= 1;
        position = position < 0 ? 0 : position;

        context.data.cursorPosition = position;

        const newSelectedToken = tokens[context.data.cursorPosition];

        context.data.selectedToken.domElement = newSelectedToken;
        context.data.selectedToken.info = getTokenInfo(newSelectedToken); 

        autoScroll(newSelectedToken);

        if (context.machineState !== "selectingMultipleTokens") {
            context.machineState = getDisplayState(context.data.selectedToken.info.type);
        }
    },
    moveCursorRight(context, payload) {
        const tokens = document.querySelectorAll(".selectable-token");

        // Alias for readability
        let position = context.data.cursorPosition;

        position += 1;
        position = position > tokens.length - 1 ? tokens.length - 1 : position;

        context.data.cursorPosition = position;
        
        const newSelectedToken = tokens[context.data.cursorPosition];

        context.data.selectedToken.domElement = newSelectedToken;
        context.data.selectedToken.info = getTokenInfo(newSelectedToken); 

        autoScroll(newSelectedToken);

        if (context.machineState !== "selectingMultipleTokens") {
            context.machineState = getDisplayState(context.data.selectedToken.info.type);
        }
    },
    removeError(context, payload) {
        const errorIndex = payload;

        context.data.selectedToken.info.errors.splice(errorIndex, 1);
        removeErrorSpan(context.data.selectedToken.domElement, errorIndex);

        if(context.data.selectedToken.info.errors.length === 0) {
          let tokenToLoad = context.data.selectedToken.domElement.previousElementSibling;
          removeRevision(context.data.selectedToken.domElement);

          if(tokenToLoad) {
            while(tokenToLoad.classList.contains("tei-space-character")) {
              tokenToLoad = tokenToLoad.previousElementSibling;
            }
            console.log(tokenToLoad.outerHTML);
            this.loadToken(context, tokenToLoad);
          } else {
            this.loadToken(context, document.querySelector(".selectable-token"));
          }
        }

        postTeiDoc({
            header: context.data.teiDoc.header,
            body: document.querySelector(".tei-container").innerHTML}
        );
    },
    render(context, payload) {
        // Do nothing, call render() to trigger component rendering.
        return;
    },
    selectMultipleTokensRequest(context, selectionStart) {
        context.data.tokenSelection.start = selectionStart;
        context.machineState = "selectingMultipleTokens";
    },
    updateCorrectedTextRequest(context, payload) {
        context.machineState = "updatingCorrectedText";
    },
    updateCursorPositionAndLoadToken(context, newPosition) {
        const tokens = document.querySelectorAll(".selectable-token");

        context.data.cursorPosition = newPosition;

        const newSelectedToken = tokens[context.data.cursorPosition];

        context.data.selectedToken.domElement = newSelectedToken;
        context.data.selectedToken.info = getTokenInfo(newSelectedToken); 

        // Add autoScroll() here

        context.machineState = getDisplayState(context.data.selectedToken.info.type);
    },
    updateError(context, errorInfo) {
        if(!isValidXTypeInput(errorInfo.xType, context.data.autocomplete.xTypeCodes)) {
            context.data.validationHints.errorSubmission =
              "Please choose an xType code from the list of suggestions."; 
           
            return;
        }
        context.data.validationHints.errorSubmission = "";

        const errorIndex = context.data.errorIndexToUpdate;

        context.data.selectedToken.info.errors[errorIndex] = errorInfo;
        updateErrorSpan(errorInfo, context.data.selectedToken.domElement, errorIndex);

        context.data.errorInputFieldValues = {xType: "", reason: "", depId: "", eId: "", idX: ""};
        context.machineState = "displayingRevision";

        postTeiDoc({
            header: context.data.teiDoc.header,
            body: document.querySelector(".tei-container").innerHTML}
        );
    },
    updateErrorRequest(context, errorIndex) {
        context.data.errorIndexToUpdate = errorIndex;
        context.data.errorInputFieldValues = context.data.selectedToken.info.errors[errorIndex];
        context.machineState = "updatingError";
    }
}