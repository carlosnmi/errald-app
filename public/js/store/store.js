import PubSub from '../lib/pubsub.js';

export default class Store {
    constructor(params) {
        let self = this;

        // Add some default objects to hold our actions, mutations and state
        self.actions = {};
        self.transitions = {};
        self.data = {};
        self.machineState = "idle";

        // Attach our PubSub module as an `events` element
        self.events = new PubSub();

        // Look in the passed params object for actions, mutations, and state
        // machine transitions that might have been passed in
        if(params.hasOwnProperty('actions')) {
            self.actions = params.actions;
        }
        
        if(params.hasOwnProperty('transitions')) {
            self.transitions = params.transitions;
        }

        if(params.hasOwnProperty('data')) {
            self.data = params.data;
        }
    }

    /**
     * A dispatcher for actions that looks in the actions 
     * collection and runs the action if it can find it
     *
     * @param {string} actionKey
     * @param {mixed} payload
     * @returns {boolean}
     * @memberof Store
     */
    dispatch(actionKey, payload) {
        console.log(`Running dispatch(${actionKey}, ${payload})`);
  
        let self = this;
        
        // Check if the action is allowed for the current state
        if(!self.transitions[self.machineState].includes(actionKey)) {
          return false;
        }

        // Run a quick check to see if the action actually exists
        // before we try to run it
        if(typeof self.actions[actionKey] !== 'function') {
          console.error(`Action "${actionKey} doesn't exist.`);
          return false;
        }
        
        // Create a console group which will contain the logs from our Proxy etc
        console.groupCollapsed(`ACTION: ${actionKey}`);
        
        // Actually call the action and pass it the Store context and whatever payload was passed
        if (!actionKey.includes("async")) {
            self.actions[actionKey](self, payload);

            console.log(`Data is stable. State: ${self.machineState}`);

            // Publish the change event for the components that are listening
            self.events.publish('stateChange', self.state);      
            
            // Close our console group to keep things nice and neat
            console.groupEnd();

            return true;
        } else {
            self.actions[actionKey](self, payload).then(() => {
                console.log(`Data is stable. State: ${self.machineState}`);

                // Publish the change event for the components that are listening
                self.events.publish('stateChange', self.state);      
                
                // Close our console group to keep things nice and neat
                console.groupEnd();

                return true;
            });;
        }

    }
}
