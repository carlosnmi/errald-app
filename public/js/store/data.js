export default {
    autocomplete: {
        xTypeCodes: {},
    },
    cursorPosition: 0,
    selectedToken:  {
        domElement: {},
        info: {},
    },
    selectableTokens: [],
    teiDoc: {
        header: "",
        body: "",
    },
    errorInputFieldValues: {
        xType: "",
        reason: "",
        depId: "",
        eId: "",
        idX: "",
    },
    errorIndexToUpdate: 0,
    tokenSelection: {
        start: 0,
    },
    validationHints: {
        correctedText: "",
        errorSubmission: "",
    },
}