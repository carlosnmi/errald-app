export default {
    appendingError: ["cancelAndGoBackToDisplayingToken", "removeError",  "appendError", "updateErrorRequest"],
    displayingWord: ["moveCursorLeft", "moveCursorRight", "selectMultipleTokensRequest", "updateCursorPositionAndLoadToken"],
    displayingPunctuation: ["moveCursorLeft", "moveCursorRight", "selectMultipleTokensRequest", "updateCursorPositionAndLoadToken"],
    displayingRevision: ["moveCursorLeft", "moveCursorRight", "appendErrorRequest", "removeError", "updateErrorRequest", "updateCorrectedTextRequest", "updateCursorPositionAndLoadToken"],
    idle: ["asyncFetchErrorCodes", "loadTeiHeader", "loadToken", "moveCursorLeft", "moveCursorRight", "updateCursorPositionAndLoadToken"],
    selectingMultipleTokens: ["cancelAndGoBackToDisplayingToken", "confirmTokenSelection", "moveCursorLeft", "moveCursorRight", "render"],
    updatingCorrectedText: ["cancelAndGoBackToDisplayingToken", "asyncUpdateCorrectedText", "removeError"],
    updatingError: ["cancelAndGoBackToDisplayingToken", "removeError", "updateError", "updateErrorRequest"],
}