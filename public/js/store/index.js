import actions from './actions.js';
import data from './data.js';
import transitions from "./transitions.js"
import Store from './store.js';

export default new Store({
    actions,
    data,
    transitions
});
