/**
 * Moves the cursor in the given input or text area to the end of the text.
 * Credit goes to CSS-Tricks.com
 * @param {*} el - A reference to an input or text area element
 */
const moveCursorToEnd = (el) => {
    if (typeof el.selectionStart == "number") {
        el.selectionStart = el.selectionEnd = el.value.length;
    } else if (typeof el.createTextRange != "undefined") {
        el.focus();
        var range = el.createTextRange();
        range.collapse(false);
        range.select();
    }
}

/**
 * Empties and populates the revision errors table
 * @param {Array} errors - The array containing the revision's errors
 * @param {*} table - A reference to the errors table
 */
const uiUpdateErrorsTable = async (errors, table) => {
  table.innerHTML =
    `<thead>` +
    `<tr><th>Type</th><th>Reason</th><th>Dep Id</th><th>Example Id</th><th>idx</th><th></th></tr>` +
    `</thead>` +
    `<tbody></tbody>`;

  const tableBody = table.children[1];

  errors.forEach(({ xType, reason, depId, eId, idX }, index) => {
    /*
    TODO:
    Need to unit test the code that adds the buttons to each table row.
    */
    const editButtonHtml =
      `<button id="btn-edit-${index}" class="waves-effect waves-light btn btn-edit">` +
      `Edit</button>`;
    const removeButtonHtml =
      `<button id="btn-remove-${index}" class="waves-effect waves-light btn btn-remove">` +
      `x</button>`;

    const processedReason = reason ? reason : "";
    const processedDepId = depId ? depId : "";
    const processedEId = eId ? eId : "";
    const processedIdX = idX ? idX : "";

    const tableRow = document.createElement("tr");
    tableRow.className = "error-row";
    tableRow.innerHTML =
      `<td>${xType}</td>` +
      `<td>${processedReason}</td>` +
      `<td>${processedDepId}</td>` +
      `<td>${processedEId}</td>` +
      `<td>${processedIdX}</td>` +
      `<td>${editButtonHtml}${removeButtonHtml}</td>`;
    tableBody.appendChild(tableRow);
  });
};

export { moveCursorToEnd, uiUpdateErrorsTable };
