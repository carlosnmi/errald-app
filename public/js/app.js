import store from "./store/index.js";
import AppState from "./components/appState.js"
import TokenCursor from "./components/tokenCursor.js"
import TokenInfo from "./components/tokenInfo.js"

import { flagSelectableTokens } from "./teiUtils.js";

const RETURN_TO_NORMAL_MODE = "Escape";

let teiHeader;

const appStateInstance = new AppState(store);
const tokenCursorInstance = new TokenCursor(store);
const tokenInfoInstance = new TokenInfo(store);

const uiLoadTeiContent = async () => {
  const uiWorkArea = document.querySelector(".ui-work-area");

  try {
    const response = await fetch("/html");

    const data = await response.json();
    teiHeader = data.teiHeader;
    console.log(teiHeader);
    uiWorkArea.innerHTML = data.html;
  } catch (e) {
    console.log(e);
  }
};

const uiUpdateTotalSentencesValue = (value) => {
  const totalSentencesSpan = document.getElementById("total-sentences-in-file");
  totalSentencesSpan.innerText = value.toString();
};

/**
 * Handles keydown events.
 * @param {*} event - The event caught by the browser.
 */
const keyDownEventHandler = (event) => {
  if (event.key === "Tab") {
    event.preventDefault();

    if (!event.shiftKey) {
      switch (document.activeElement.id) {
        case "xtype-input":
          document.getElementById("reason-input").focus();
          break;
        case "reason-input":
          document.getElementById("dep-id-input").focus();
          break;
        case "dep-id-input":
          document.getElementById("example-id-input").focus();
          break;
        case "example-id-input":
          document.getElementById("idx-input").focus();
          break;
        case "idx-input":
          document.getElementById("btn-confirm").focus();
          break;
        case "btn-confirm":
          document.getElementById("xtype-input").focus();
          break;
        default:
          break;
      }
    } else {
      switch (document.activeElement.id) {
        case "xtype-input":
          document.getElementById("btn-confirm").focus();
          break;
        case "reason-input":
          document.getElementById("xtype-input").focus();
          break;
        case "dep-id-input":
          document.getElementById("reason-input").focus();
          break;
        case "example-id-input":
          document.getElementById("dep-id-input").focus();
          break;
        case "idx-input":
          document.getElementById("example-id-input").focus();
          break;
        case "btn-confirm":
          document.getElementById("idx-input").focus();
          break;
        default:
          break;
      }
    }
  }
};

/**
 * Handles keyup events.
 * @param {*} event - The event caught by the browser.
 */
const keyUpEventHandler = (event) => {
  switch (event.key) {
    case RETURN_TO_NORMAL_MODE:
      store.dispatch("cancelAndGoBackToDisplayingToken", null);
      break;
    default:
      break;
  }
};

document.addEventListener("DOMContentLoaded", function () {
  let modalElements = document.querySelectorAll(".modal");
  let modalInstances = M.Modal.init(modalElements, { dismissible: true });

  document.onkeyup = keyUpEventHandler;
  document.onkeydown = keyDownEventHandler;

  uiLoadTeiContent().then(() => {
    appStateInstance.render();

    const totalSentencesInFile = document.querySelectorAll(".tei-s").length;
    uiUpdateTotalSentencesValue(totalSentencesInFile);

    flagSelectableTokens();
    tokenCursorInstance.render();
    store.dispatch("asyncFetchErrorCodes", null);
    store.dispatch("loadTeiHeader", teiHeader);
    store.dispatch("loadToken", document.querySelector(".selectable-token"));
  });
});

/*
TODO:

 - Create revisions for single tokens.
 - Create revisions for multiple contiguous tokens.
   - Add a target token class to the elements I want to highlight when the
     cursor moves past them. Remove the class when I'm no longer selecting.
   - I probably need a "selectingToken" state.
 - Prevent the user from entering invalid TEI XML strings when editing the
   <corrected> elements in each revision.
   - The main restrictions are:
     1. Don't allow entering XML elements other than <w> or <c>.
     2. Don't allow entering empty <w> or <c> elements.
     3. Don't allow nested elements. That is, the entered <w> or <c> elements
        may only have text as children.
     4. Don't allow blank spaces inside <w> or <c> elements.
 - Highlight which revision the depId is refering too as the user types it in.
 - Add new revision spans for single words or punctuation marks.
 - Add new revision spans for multiple words and or punctuation marks.
   - This would involve parsing each word or punctuation into the corresponding
     HTML. Actually, that's not too hard. I just move those elements into the
     new revision's original span. Then I have to allow the user to input the
     corrected text into the UI and then parse that text into w, c and blank
     space elements.
*/
