/**
 * Returns the element's positional offset from the top of the viewport.
 *
 * Large positive numbers indicate the element is closer to the bottom of the
 * screen. Negative numbers indicate the element is above the viewport and thus
 * invisible to the user.
 * @param {*} elem - An HTML DOM element.
 * @returns {Number}
 */
const getTopOffset = (elem) => {
  const rect = elem.getBoundingClientRect();
  return rect.top;
};

/**
 * Returns -1 if element is above viewport, 0 if it's within viewport and 1 if
 * it's below viewport.
 * @param {*} elem - An HTML DOM element.
 * @returns {Number}
 *
 * @example
 *
 * Here the element is above the viewport and thus not visible to the user.
 * Should return -1.
 *
 *   <element>
 * -------------
 * |           |  <-- Viewport
 * -------------
 *
 * Here the element is within the viewport and thus visible to the user.
 * Should return 0.
 *
 * -------------
 * | <element> |  <-- Viewport
 * -------------
 *
 * Here the element is below the viewport and thus invisible to the user.
 * Should return 1.
 *
 * -------------
 * |           |  <-- Viewport
 * -------------
 *   <element>
 */
const getViewportPositionRelationship = (elem) => {
  if (getTopOffset(elem) < 0) {
      return -1;
  } else if (getTopOffset(elem) > window.innerHeight) {
      return 1;
  }

  return 0;
};

const autoScroll = (selectedToken) => {
  const EXTRA_SCROLL_AMOUNT_FOR_AESTHETICS = 42;

  if (getViewportPositionRelationship(selectedToken) < 0) {
    const scrollAmount =
      getTopOffset(selectedToken) -
      EXTRA_SCROLL_AMOUNT_FOR_AESTHETICS;

    window.scrollBy(0, scrollAmount);
  }
  if (getViewportPositionRelationship(selectedToken) > 0) {
    const scrollAmount =
      getTopOffset(selectedToken) -
      window.innerHeight +
      EXTRA_SCROLL_AMOUNT_FOR_AESTHETICS;

    window.scrollBy(0, scrollAmount);
  }
};

export { autoScroll };