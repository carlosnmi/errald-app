const ORIGINAL_SPAN_INDEX = 0;
const CORRECTED_SPAN_INDEX = 1;
const ERRORS_SPAN_INDEX = 2;

/**
 * @typedef {Object} RevisionText
 * @property {String} original - The original text
 * @property {String} corrected - The corrected text
 */

/**
 * Encodes the given error info into a new ".tei-error" span and then appends it
 * to the given ".tei-revision" span.
 * @param {Object} errorInfo
 * @param {*} revisionSpan
 */
const appendErrorSpan = ({ xType, reason, depId, eId, idX }, revisionSpan) => {
  if (!xType) {
    throw new Error("xtype must be a non-empty string");
  }

  if (!revisionSpan) {
    throw new Error("Revision is null or undefined");
  }

  if (!revisionSpan.children.length) {
    throw new Error("Revision is childless");
  }

  const teiErrors = revisionSpan.children[2];
  const teiError = document.createElement("span");

  teiError.className = `tei-error tei-error-xtype-${xType}`;

  if (reason) {
    teiError.className += ` tei-error-reason-${reason}`;
  }

  if (depId) {
    teiError.className += ` tei-error-depId-${depId}`;
  }

  if (eId) {
    teiError.className += ` tei-error-eid-${eId}`;
  }

  if (idX) {
    teiError.className += ` tei-error-idx-${idX}`;
  }

  teiErrors.appendChild(teiError);
};

/**
 * Updates error span defined by given revision and index
 * @param {Object} errorInfo - The new error information
 * @param {*} revision - The revision containing the error
 * @param {*} index - The error's index within the revision
 */
const updateErrorSpan = (
  { xType, reason, depId, eId, idX },
  revision,
  index
) => {
  const errorSpan = revision.children[2].children[index];

  errorSpan.className = `tei-error tei-error-xtype-${xType}`;

  if (reason) {
    errorSpan.className += ` tei-error-reason-${reason}`;
  }

  if (depId) {
    errorSpan.className += ` tei-error-depId-${depId}`;
  }

  if (eId) {
    errorSpan.className += ` tei-error-eid-${eId}`;
  }

  if (idX) {
    errorSpan.className += ` tei-error-idx-${idX}`;
  }
};

/**
 * Async wrapper around updateErrorSpan()
 * @param {Object} errorInfo - The new error information
 * @param {*} revision - The revision containing the error
 * @param {*} index - The error's index within the revision
 */
const asyncUpdateErrorSpan = async (
  { xType, reason, depId, eId, idX },
  revision,
  index
) => {
  updateErrorSpan({ xType, reason, depId, eId, idX }, revision, index);
};

/**
 * Returns a '.tei-revision' span created using the contents of the selected
 * tokens. Note that the given tokens will not be modified in any way.
 * @param {String} id - The desired revision id.
 * @param {*} selectedTokens - A NodeList with the selected tokens
 * @returns {*} A DOM element
 */
const createRevision = (id, selectedTokens) => {
  const totalTokens = selectedTokens.length;
  const teiSpaceCharacterSpan = `<span class="tei-space-character"> </span>`;

  const revision = document.createElement("span");
  revision.className = "tei-revision selectable-token";
  revision.id = `tei-revision-id-${id}`;

  let originalChildren = "";

  selectedTokens.forEach((token, index) => {
    originalChildren += token.outerHTML;

    // Append a ".tei-space-character" span for every token except the last one.
    if (index < totalTokens - 1) {
      // Make an exception and don't append a space character to words followed
      // by a punctuation mark.
      if (!selectedTokens[index + 1].classList.contains("tei-c")) {
        originalChildren += teiSpaceCharacterSpan;
      }
    }
  });

  revision.innerHTML =
    `<span class="tei-original">${originalChildren}</span>` +
    `<span class="tei-corrected"></span>` +
    `<span class="tei-errors"></span>`;
  
  const revisionDescendants = revision.querySelectorAll("span");

  revisionDescendants.forEach((descendant) => {
    descendant.classList.remove("selectable-token", "selected-token");
  });

  return revision;
}

/**
 * Returns a new HTML span with a class of "tei-w" and the given text content.
 *
 * You must only pass a single word into the function.
 *
 * @param {String} textContent - The word to encode into the HTML span
 * @returns {*}
 */
const createWordElement = (textContent) => {
  if (textContent.trim().includes(" ")) {
    throw new Error("Received more than one word");
  }

  const word = document.createElement("span");
  word.className = "tei-w";
  word.textContent = textContent;

  return word;
};

/**
 * Adds selectable-token class to all words, punctuation marks and revisions,
 * except for the words and punctuation marks inside revisions.
 */
const flagSelectableTokens = () => {
  const wordsPunctuationAndRevisions = document.querySelectorAll(
    ".tei-w, .tei-c, .tei-revision"
  );
  wordsPunctuationAndRevisions.forEach((elem) => {
    if (
      !elem.parentNode.classList.contains("tei-original") &&
      !elem.parentNode.classList.contains("tei-corrected")
    ) {
      elem.classList.add("selectable-token");
    }
  });
};

/**
 * Returns the XML attribute value encoded in the corresponding HTML element's class.
 * @param {*} element - A DOM element representing a TEI XML element
 * @param {String} classValuePrefix - The string prefixing the value
 *
 * @example
 *
 * Element represents <span class="tei-error-xtype-pizza"> . . . </span>
 *
 * getClassEncodedAttribute(element, "tei-revision-xtype-");
 * // Returns "pizza"
 *
 * @returns {String} The queried value or null if not encoded in the element
 */
const getClassEncodedAttribute = (element, classValuePrefix) => {
  const classValues = element.className.split(" ");

  const desiredArrayItem = classValues.find((value) =>
    value.includes(classValuePrefix)
  );

  return desiredArrayItem
    ? desiredArrayItem.replace(classValuePrefix, "")
    : null;
};

/**
 * Returns the XML attribute value encoded in the corresponding HTML element's id.
 * @param {*} element - A DOM element representing a TEI XML element
 * @param {String} idValuePrefix - The string prefixing the value
 *
 * @example
 * Element represents <span id="tei-revision-id-5"> . . . </span>
 *
 * getIdEncodedAttribute(element, "tei-revision-id-");
 * // Returns "5"
 *
 * @returns {String} - The queried value or null if not encoded in the element
 */
const getIdEncodedAttribute = (element, idValuePrefix) => {
  return element.id.includes(idValuePrefix)
    ? element.id.replace(idValuePrefix, "")
    : null;
};

/**
 * Returns the given revision's original and corrected text strings.
 * @param {*} revision - The revision
 * @returns {RevisionText} The original and corrected text strings
 */
const getOriginalAndCorrectedText = (revision) => {
  const originalSpan = revision.children[ORIGINAL_SPAN_INDEX];
  let originalText = null;
  if (originalSpan.children.length) {
    originalText = originalSpan.textContent;
  }

  const correctedSpan = revision.children[CORRECTED_SPAN_INDEX];
  let correctedText = null;
  if (correctedSpan.children.length) {
    correctedText = correctedSpan.textContent;
  }

  return { original: originalText, corrected: correctedText };
};

/**
 * Returns an object with the revision's information.
 * @param {*} revision - The revision span you want to decode.
 * @returns {Object} An object with the revision's information.
 */
const getRevisionInfo = (revision) => {
  const id = getIdEncodedAttribute(revision, "tei-revision-id-");

  const sentence = revision.parentNode;
  const paragraph = sentence.parentNode;

  const paragraphNumber = getIdEncodedAttribute(paragraph, "tei-p-n-");
  const sentenceNumber = getIdEncodedAttribute(sentence, "tei-s-n-");

  const originalAndCorrectedText = getOriginalAndCorrectedText(revision);

  const totalErrors = revision.children[2].childElementCount;
  const errors = [];
  for (let index = 0; index < totalErrors; index += 1) {
    const currentErrorElement = revision.children[2].children[index];

    const xType = getClassEncodedAttribute(
      currentErrorElement,
      "tei-error-xtype-"
    );

    const reason = getClassEncodedAttribute(
      currentErrorElement,
      "tei-error-reason-"
    );

    const depId = getClassEncodedAttribute(
      currentErrorElement,
      "tei-error-depId-"
    );

    const eId = getClassEncodedAttribute(currentErrorElement, "tei-error-eid-");

    const idX = getClassEncodedAttribute(currentErrorElement, "tei-error-idx-");

    errors.push({ xType, reason, depId, eId, idX });
  }

  return {
    id,
    paragraphNumber,
    sentenceNumber,
    originalText: originalAndCorrectedText.original,
    correctedText: originalAndCorrectedText.corrected,
    errors,
  };
};

/**
 * Returns a NodeList with the tokens that have a ".selectable-token" class.
 * @returns {NodeList}
 */
const getSelectableTokens = () => {
  return document.querySelectorAll(".selectable-token");
};

const getTokenInfo = (token) => {
  let info = {};

  if (token.classList.contains("tei-w")) {
    info.type = "word";
    info.content = token.textContent;
  } else if (token.classList.contains("tei-c")) {
    info.type = "punctuation";
    info.content = token.textContent;
  } else {
    info.type = "revision";

    const revisionInfoEntries = Object.entries(getRevisionInfo(token));

    for (const [key, value] of revisionInfoEntries) {
      info[key] = value;
    }
  }

  return info;
};

/**
 * Returns true if given xType input is included in the given error codes object
 * @param {String} input - The input string to validate
 * @param {Object} errorCodes - The object containing the valid xType codes
 * @returns {Boolean}
 */
const isValidXTypeInput = (input, errorCodes) => {
  let result = false;

  Object.keys(errorCodes).forEach((key) => {
    if (key === input) {
      result = true;
    }
  });

  return result;
};

/**
 * Removes the error span for the given revision at the given error index
 * @param {*} revision - The enclosing revision span
 * @param {Number} index - The error span's index (zero-indexed value)
 * @example
 *
 * // Remove the third error span in the revision.
 * uiRemoveErrorSpan(revision, 2)
 */
const removeErrorSpan = (revision, index) => {
  const errorSpanToRemove = revision.children[2].children[index];
  errorSpanToRemove.parentNode.removeChild(errorSpanToRemove);
};

/**
 * Replaces the revision span with the elements inside its '.tei-original' span.
 * @param {*} revision - The revision span you want to remove
 */
const removeRevision = (revision) => {
  const enclosingSentence = revision.parentElement;
  const originalSpansChildren = revision.querySelectorAll(".tei-original > span");

  originalSpansChildren.forEach((child) => {
    if(!child.classList.contains("tei-space-character")) {
      child.classList.add("selectable-token");
    }

    enclosingSentence.insertBefore(child, revision);
  });

  enclosingSentence.removeChild(revision);
};

/**
 * Removes the given DOM elements from the HTML tree and it replaces them with the
 * given DOM element.
 * @param {*} oldElements - A NodeList reference with the elements you want to replace.
 * @param {*} newElement - The DOM element you want to add to the HTML tree.
 */
const replaceElements = (oldElements, newElement) => {
  const newElmentsParentElement = oldElements[0].parentElement;

  newElmentsParentElement.insertBefore(newElement, oldElements[0]);

  oldElements.forEach((element) => {
    element.parentElement.removeChild(element);
  })
}

export {
  appendErrorSpan,
  asyncUpdateErrorSpan,
  createRevision,
  createWordElement,
  flagSelectableTokens,
  getClassEncodedAttribute,
  getIdEncodedAttribute,
  getOriginalAndCorrectedText,
  getRevisionInfo,
  getSelectableTokens,
  getTokenInfo,
  isValidXTypeInput,
  removeRevision,
  removeErrorSpan,
  replaceElements,
  updateErrorSpan,
};
