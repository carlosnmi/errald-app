import Component from "../lib/component.js";
import store from "../store/index.js";

const CONFIRM_TOKEN_SELECTION = "Enter";
const MOVE_CURSOR_LEFT = "j";
const MOVE_CURSOR_RIGHT = "k";
const SELECT_MULTIPLE_TOKENS = "i";

/**
 * Adds a class of "hovered-token" to elements with a class of
 * "selectable-token".
 * @param {*} evt - The detected event.
 */
const addHoveredTokenClass = (evt) => {
  const targetElement = evt.target;
  if (targetElement.classList.contains("selectable-token")) {
    targetElement.classList.add("hovered-token");
  }
};

/**
 * Removes the "hovered-token" class from the target element if the element has
 * it.
 * @param {*} evt - The detected event.
 */
const removeHoveredTokenClass = (evt) => {
  const targetElement = evt.target;
  if (targetElement.classList.contains("hovered-token")) {
    targetElement.classList.remove("hovered-token");
  }
};

export default class TokenCursor extends Component {
  constructor() {
    super({
      store,
      element: document.querySelector(".ui-work-area") 
    });

    this.position = 0;

    document.body.addEventListener("keyup", ({key}) => {
      const tokens = this.element.querySelectorAll(".selectable-token");

      switch(key) {
        case CONFIRM_TOKEN_SELECTION:
          if (store.machineState === "selectingMultipleTokens") {
            store.dispatch("confirmTokenSelection", null);
          }
          break;
        case MOVE_CURSOR_LEFT:
          switch(store.machineState) {
            case "idle":
            case "displayingWord":
            case "displayingPunctuation":
            case "displayingRevision":
              store.dispatch("moveCursorLeft", null);
              break;
            case "selectingMultipleTokens":
              if (store.data.cursorPosition - 1 > 0) {
                if (tokens[store.data.cursorPosition - 1].classList.contains("tei-revision")) {
                  break;
                }
              }

              store.dispatch("moveCursorLeft", null);
              break;
            default:
              break;
          }
          break;
        case MOVE_CURSOR_RIGHT:          
          switch(store.machineState) {
            case "idle":
            case "displayingWord":
            case "displayingPunctuation":
            case "displayingRevision":
              store.dispatch("moveCursorRight", null);
              break;
            case "selectingMultipleTokens":
              if (store.data.cursorPosition + 1 < tokens.length) {
                if (tokens[store.data.cursorPosition + 1].classList.contains("tei-revision")) {
                  break;
                }
              }

              store.dispatch("moveCursorRight", null);
              break;
            default:
              break;
          }
          break;
        case SELECT_MULTIPLE_TOKENS:
          switch(store.machineState) {
            case "displayingWord":
            case "displayingPunctuation":
              store.dispatch("selectMultipleTokensRequest", store.data.cursorPosition);
              break;
            case "displayingRevision":
              store.dispatch("appendErrorRequest", null);
              break;
            default:
              break;
          }
          break;
        default:
          break;
      }
    });

    this.element.addEventListener("mouseover", addHoveredTokenClass);
    this.element.addEventListener("mouseout", removeHoveredTokenClass);

    this.element.addEventListener("click", (evt) => {
      const targetElement = evt.target;
      console.log(targetElement.outerHTML);

      if (
        targetElement.classList.contains("tei-w") ||
        targetElement.classList.contains("tei-c") ||
        targetElement.classList.contains("tei-hi")
      ) {
        const tokens = this.element.querySelectorAll(".selectable-token");

        for (let index = 0; index < tokens.length; index += 1) {
          if (tokens[index].classList.contains("hovered-token")) {
            tokens[index].classList.remove("hovered-token");
            store.dispatch("updateCursorPositionAndLoadToken", index);
            break;
          }
        }
      }
    });
  }

  render() {
    const tokens = this.element.querySelectorAll(".selectable-token");

    const selectedTokens = this.element.querySelectorAll(".selected-token");
    selectedTokens.forEach((token) => {
      token.classList.remove("selected-token");
    });

    switch(store.machineState) {
      case "appendingError":
      case "idle":
      case "displayingWord":
      case "displayingPunctuation":
      case "displayingRevision":
      case "updatingCorrectedText":
      case "updatingError":
        tokens[store.data.cursorPosition].classList.add("selected-token");
        break;
      case "selectingMultipleTokens":
        console.log("tokenCursor: Rendering while selecting multiple tokens.")
        const selectionStart = store.data.tokenSelection.start;
        const selectionEnd = store.data.cursorPosition;

        const indexStart = selectionEnd >= selectionStart ? selectionStart : selectionEnd;
        const indexEnd = selectionEnd >= selectionStart? selectionEnd: selectionStart;

        for(let index = indexStart; index <= indexEnd; index += 1) {
          tokens[index].classList.add("selected-token");
        }
      break;
     default:
        break;
    }

    /*
    NOTE:
    Add and remove the "hovered-token" class using "mouseenter" and "mouseleave"
    event listeners on all selectable elements.

    It's not enough to add "mouseover" and "mouseout" event listeners to the
    ui-work-area and rely on event delegation to apply event listeners to all
    words, punctuation marks and revisions because these elements contain
    child elements.
   
    For example, revisions contain many layers of child elements. So placing the
    cursor over a revision will trigger a "mouseover" event that is immediately
    followed by a "mouseout" event. At least in Google Chrome, the net effect is
    that the "mouseout" events will cancel out the "mouseover" events and the
    user will never see revisions being highlighted when the mouse enters their
    space.

    These event listeners need to be added every time render() is called since
    elements are dynamically being added and removed from the ui-work-area.
    */
    tokens.forEach((token, index) => {
      token.addEventListener("mouseenter", addHoveredTokenClass);
      token.addEventListener("mouseleave", removeHoveredTokenClass);
    });

    /*
    TODO: Need to be able to select italicized elements by clicking.
    */
  }
}
