import Component from "../lib/component.js";
import store from "../store/index.js";

import {
  getRevisionInfo,
  isValidXTypeInput,
} from "../teiUtils.js";

import { moveCursorToEnd } from "../uiUtils.js";

/*
Note on "specialXTypeCases":

Array containing all the xType codes that should be visualized as invalid while
still being included in errorCodes.json and allowed during error update and
submission.
*/
const specialXTypeCases = ["xxx"];

/**
 * Returns the HTML representation for the given revision and actionables.
 * @param {*} token - A DOM element representing the selected revision token.
 * @returns {String} The TokenInfo UI component's HTML.
 */
const getRevisionInfoHtml = (token) => {
  let errorRows;

  token.errors.forEach(({ xType, reason, depId, eId, idX }, index) => {
    const editButtonHtml =
      `<button id="btn-edit-${index}" class="waves-effect waves-light btn btn-edit">` +
      `Edit</button>`;
    const removeButtonHtml =
      `<button id="btn-remove-${index}" class="waves-effect waves-light btn btn-remove">` +
      `x</button>`;

    const isXTypeValid =
      isValidXTypeInput(xType, store.data.autocomplete.xTypeCodes) &&
      !specialXTypeCases.includes(xType);

    const processedXType = isXTypeValid ? xType : `${xType} !`;
    const processedReason = reason ? reason : "";
    const processedDepId = depId ? depId : "";
    const processedEId = eId ? eId : "";
    const processedIdX = idX ? idX : "";

    let highlight = "";
    let xTypeTdClass = "";

    // Leading white space is intentional when properties are set below.
    if (store.machineState === "updatingError" && index === store.data.errorIndexToUpdate) {
      highlight = " highlight-error-row-to-update"; 
      xTypeTdClass = ` class="invalid-xtype-table-cell"`;
    } else {
      if(!isXTypeValid) {
        highlight = " highlight-error-row-invalid-xtype";
        xTypeTdClass = ` class="invalid-xtype-table-cell"`;
      }
    }

    const errorRow =
    `<tr class="error-row${highlight}">
      <td${xTypeTdClass}>${processedXType}</td>
      <td>${processedReason}</td>
      <td>${processedDepId}</td>
      <td>${processedEId}</td>
      <td>${processedIdX}</td>
      <td style="text-align:right">${editButtonHtml}${removeButtonHtml}</td>
    </tr>
    `;
    errorRows += errorRow;
  });
  
  const errorsTable = `
    <table class="errors-table">
      <thead>
        <tr>
          <th>Type</th>
          <th>Reason</th>
          <th>Dep Id</th>
          <th>Example Id</th>
          <th>idx</th>
          <th></th>
        </tr>
      </thead>
      <tbody>${errorRows}</tbody>
    </table>`;
  
  const errorInputFieldValues = store.data.errorInputFieldValues;

  let displayedXType = errorInputFieldValues.xType || "";
  if(store.machineState === "updatingError") {
    if (
      !isValidXTypeInput(errorInputFieldValues.xType, store.data.autocomplete.xTypeCodes) ||
      specialXTypeCases.includes(errorInputFieldValues.xType)
    ) {
      displayedXType = "";
    }
  }

  return `
    <h2 class="revision-header">Revision: ${token.id}</h2>
    <p><b>Original:</b></p>
    <p>${token.originalText}</p>
    <p><b>Corrected:</b></p>
    <div class="input-field">
      <input type="text" id="corrected-input" value="${token.correctedText || ""}">
    </div>
    <p class="red-text text-darken-4">${store.data.validationHints.correctedText}</p>
    <div class="btn-update-corrected-text-container">
      <a id="btn-update-corrected-text" class="waves-effect waves-light btn"><i class="material-icons left">check</i>Save</a>
    </div>
    <h4>Errors</h4>
    <div class="add-error-container">
      <div class="input-field">
        <p class="grey-text text-darken-1">Type</p>
        <input type="text" id="xtype-input" tabindex="1" class="error-input autocomplete" value="${displayedXType}">
      </div>
      <div class="input-field">
        <p class="grey-text text-darken-1">Reason</p>
        <input type="text" id="reason-input" tabindex="2" class="error-input" value="${errorInputFieldValues.reason || ""}">
      </div>
      <div class="input-field">
        <p class="grey-text text-darken-1">Dep Id</p>
        <input type="text" id="dep-id-input" tabindex="3" class="error-input" value="${errorInputFieldValues.depId || ""}">
      </div>
      <div class="input-field">
        <p class="grey-text text-darken-1">Example Id</p>
        <input type="text" id="example-id-input" tabindex="4" class="error-input" value="${errorInputFieldValues.eId || ""}">
      </div>
      <div class="input-field">
        <p class="grey-text text-darken-1">idx</p>
        <input type="text" id="idx-input" tabindex="5" class="error-input" value="${errorInputFieldValues.idX || ""}">
      </div>
      <div>
        <button id="btn-confirm" tabindex="6" class="waves-effect waves-light btn">
          <i class="material-icons left">add</i>Add
        </button>
      </div>
    </div>
    <p class="red-text text-darken-4">${store.data.validationHints.errorSubmission || ""}</p>
    ${errorsTable}
    `;
}

const getWordInfoHtml = (token) => {
  return `
    <h2>Word</h2>
    <p>${token.content}</p>
    `;
};

const getPunctuationInfoHtml = (token) => {
  return `
    <h2>Punctuation</h2>
    <p>${token.content}</p>
    `;
};

export default class TokenInfo extends Component {
  constructor() {
    super({
      store,
      element: document.querySelector(".token-info")
    });
  }

  render() {
    /*
    TODO:

    Highlight the background for the section of the component corresponding to
    the active state. For example, highlight the correctedText container when
    updating the corrected text or highlight the container for the error input
    fields when appending an error, etc.

    Add a button to cancel the current action and go back to displaying the
    token's info.
     */
    const token = store.data.selectedToken.info;

    if (!token.type) {
      return;
    }

    switch(token.type) {
      case "revision":
        this.element.innerHTML = getRevisionInfoHtml(token);

        let autocompleteElements = this.element.querySelectorAll(".autocomplete");
        let autocompleteInstances = M.Autocomplete.init(autocompleteElements, {
          data: store.data.autocomplete.xTypeCodes,
        });

        const correctedInput = this.element.querySelector("#corrected-input");
        const xTypeInput = this.element.querySelector("#xtype-input");

        switch(store.machineState) {
          case "appendingError":
            xTypeInput.focus();
            
            if(store.data.validationHints.errorSubmission) {
              moveCursorToEnd(xTypeInput);
            }

            break;
          case "updatingCorrectedText":
            correctedInput.focus();
            moveCursorToEnd(correctedInput);
            break;
          case "updatingError":
            xTypeInput.focus();
            moveCursorToEnd(xTypeInput);
            break;
          default:
            break;
        }

        const submitError = () => {
          const xType = xTypeInput.value;
          const reason = this.element.querySelector("#reason-input").value;
          const depId = this.element.querySelector("#dep-id-input").value;
          const eId = this.element.querySelector("#example-id-input").value;
          const idX = this.element.querySelector("#idx-input").value;

          if (store.machineState === "appendingError") {
            store.dispatch("appendError", {xType, reason, depId, eId, idX});
          } else if (store.machineState === "updatingError") {
            store.dispatch("updateError", {xType, reason, depId, eId, idX});
          }
        };

        correctedInput.addEventListener("click", () => {
          store.dispatch("updateCorrectedTextRequest", null);
        });

        correctedInput.addEventListener("keyup", ({key}) => {
          if (key === "Enter") {
            store.dispatch("asyncUpdateCorrectedText", correctedInput.value || "");
          }
        });

        this.element.querySelector("#btn-update-corrected-text")
          .addEventListener("click", (event) => {
            event.preventDefault();

            store.dispatch("asyncUpdateCorrectedText", correctedInput.value || "");
          });

        this.element.querySelector("#btn-confirm")
          .addEventListener("click", submitError);
        
        this.element.querySelectorAll(".btn-remove").forEach((button, index) => {
          button.addEventListener("click", () => {
            store.dispatch("removeError", index);
          });
        });

        this.element.querySelectorAll(".btn-edit").forEach((button, index) => {
          button.addEventListener("click", () => {
            store.dispatch("updateErrorRequest", index);
            this.element.querySelector("#xtype-input").focus(); 
          });
        });

        this.element.querySelectorAll(".error-input")
          .forEach((input) => {
            input.addEventListener("click", () => {
              store.dispatch("appendErrorRequest", null);
            });

            if (input.id !== "xtype-input") {
              input.addEventListener("keyup", ({key}) => {
                if (key === "Enter") {
                  submitError();
                }
              });
            }

            /*
            TODO:
            Work on the keyup event listener so pressing the enter key only
            triggers an error submission when the user is not pressing enter
            to confirm an autocomplete suggestion.
            */
          });
        break;
      case "word":
        this.element.innerHTML = getWordInfoHtml(token);
        break;
      case "punctuation":
        this.element.innerHTML = getPunctuationInfoHtml(token);
        break;
      default:
        break;
    }
  }
}

/**
 * Returns an object with the given token's information.
 * @param {*} token - A DOM element representing the selected revision token.
 * @returns {Object} The token's information.
 */
const getTokenInfo = (token) => {
  let info = {};

  if (token.classList.contains("tei-w")) {
    info.type = "word";
    info.content = token.textContent;
  } else if (token.classList.contains("tei-c")) {
    info.type = "punctuation";
    info.content = token.textContent;
  } else {
    info.type = "revision";

    const revisionInfoEntries = Object.entries(getRevisionInfo(token));

    for (const [key, value] of revisionInfoEntries) {
      info[key] = value;
    }
  }

  return info;
};
