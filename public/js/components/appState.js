import Component from "../lib/component.js";
import store from "../store/index.js";

export default class AppState extends Component {
  constructor() {
    super({
      store,
      element: document.querySelector(".js-app-state")
    });
  }

  render() {
    switch(store.machineState) {
      case "idle":
        this.element.textContent = "INITIALIZING...";
        break;
      case "displayingWord":
        this.element.textContent = "NORMAL";
        break;
      case "displayingPunctuation":
        this.element.textContent = "NORMAL";
        break;
      case "displayingRevision":
        this.element.textContent = "NORMAL";
        break;
      case "appendingError":
        this.element.textContent = "INSERT (Error entry)";
        break;
      case "selectingMultipleTokens":
        this.element.textContent = "SELECTING TOKENS";
        break;
      case "updatingCorrectedText":
        this.element.textContent = "EDIT (Corrected text)";
        break;
      case "updatingError":
        this.element.textContent = "EDIT (Error entry)";
        break;
      default:
        this.element.textContent = "";
        break;
    }
  }
}
