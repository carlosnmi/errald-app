// const fetchTeiDoc = async () => {
//     const response = await fetch("/html");
//     const data = await response.json();

//     return data;
// };

const postTeiDoc = async ({header, body}) => {
  const data = { teiHeader: header, html: body };

  try {
    const response = await fetch("/xml", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  } catch (e) {
    console.log(e);
  }
};

const xmlTokensToHtml = async (teiXmlString) => {
  const response = await fetch("/xml-tokens-to-html", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ xml: teiXmlString || "" }),
  });

  return await response.json();
};

export {
  postTeiDoc,
  xmlTokensToHtml
};