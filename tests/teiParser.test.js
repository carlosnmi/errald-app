/*
Warning 1: Cheerior's children()
--------------------------------
Cheerio's children() method gets applied to all descendants, not just the
element's immediate descendants. This is a known bug.

https://github.com/cheeriojs/cheerio/issues/17

Example XML:

`<p n="1"><s n="1"><w>Hello</w><w>world</w></s><s n="2"><w>Bye</w><w>for</w><w>now</w></s></p>`

The following call should return 2, but instead it returns 6. The first sentence only contains two children, the word tags for `Hello` and `world`.

`childrenInFirstSentence = $("s").children().length //=> 6`

Also, if you iterate over the collection returned by `.children()` you do get the words `Bye`, `for`, and `now` instead of just `Hello` and `world`.

One workaround is to query the actual length of the DOM node object's children array.

Warning 2: Cheerio's siblings()
-------------------------------
Cheerio's siblings() method only returns elements that share the same parent. So
you can't use it to count the number of elements at the same tree depth level.

Try it yourself:

Load this XML tree:

<parent><child1/><child2/></parent>

Then run this code:

$("child1").get(0).siblings().length

The result will be 1 as expected.

Now load this XML tree instead:

<child1/><child2/>

$("child1").get(0).siblings().length

The result will be 0 since child1 and child2 don't share the same parent.

Funny enough, in the second example, asking for child1's nextSibling property
will still return a reference to child2. And asking for child2's previousSibling
property will also still return a reference to child1.

So, be careful Cheerio is a bit inconsistent with its naming conventions.
*/

const cheerio = require("cheerio");

const {
  getClassEncodedAttribute,
  getIdEncodedAttribute,
  getEId,
  getReason,
  getXType,
  toHtml,
  toXml,
  InvalidInputError,
} = require("../src/teiParser");

const { html } = require("cheerio");

describe("getEId()", () => {
  test("returns eid value encoded in class value.", () => {
    const inputHtml = `<span class="tei-error tei-error-xtype-cheese tei-error-eid-pizza" />`;

    const $ = cheerio.load(inputHtml);

    const teiErrorElement = $(".tei-error").get(0);

    const eId = getEId(teiErrorElement);

    expect(eId).toBe("pizza");
  });

  test("returns expected value regardless of its position in the class values string", () => {
    /*
    Example, getEId() should return "pizza" for both of these HTML elements:

    <span class="tei-error tei-error-eid-joke tei-error-xtype-pizza">
                                     ^
    <span class="tei-error tei-error-xtype-pizza tei-error-eid-joke">
                                                           ^
    */
    const inputHtml = `<span class="tei-error tei-error-something tei-error-xtype-pizza tei-error-eid-joke" />`;

    const $ = cheerio.load(inputHtml);

    const teiErrorElement = $(".tei-error").get(0);

    const eId = getEId(teiErrorElement);

    expect(eId).toBe("joke");
  });
});

describe("getClassEncodedAttribute()", () => {
  test("returns attribute value encoded in class value.", () => {
    const inputHtml = `<span class="tei-error tei-error-xtype-cheese tei-error-eid-pizza tei-error-reason-because" />`;

    const $ = cheerio.load(inputHtml);

    const teiErrorElement = $(".tei-error").get(0);

    const value = getClassEncodedAttribute(
      teiErrorElement,
      "tei-error-reason-"
    );

    expect(value).toBe("because");
  });

  test("returns expected value regardless of its position in the class values string", () => {
    /*
    Example, getClassEncodedAttribute() should return "pizza" for both of these HTML elements:

    <span class="tei-error tei-error-eid-pizza tei-error-xtype-joke">
                                         ^
    <span class="tei-error tei-error-xtype-joke tei-error-eid-pizza">
                                                              ^
    */
    const inputHtml = `<span class="tei-error tei-error-something tei-error-xtype-joke tei-error-eid-pizza" />`;

    const $ = cheerio.load(inputHtml);

    const teiErrorElement = $(".tei-error").get(0);

    const value = getClassEncodedAttribute(teiErrorElement, "tei-error-eid-");

    expect(value).toBe("pizza");
  });

  test("Returns null if the queried value was not encoded into the element's class attribute.", () => {
    const inputHtml = `<span class="tei-error tei-error-xtype-pizza" />`;

    const $ = cheerio.load(inputHtml);

    const teiErrorElement = $(".tei-error").get(0);

    const value = getClassEncodedAttribute(
      teiErrorElement,
      "tei-error-reason-"
    );

    expect(value).toBe(null);
  });
});

describe("getIdEncodedAttribute()", () => {
  test("Returns attribute value encoded in id value.", () => {
    const inputHtml = `<span class="tei-revision" id="tei-revision-id-pizza" />`;

    const $ = cheerio.load(inputHtml);

    const teiRevisionElement = $(".tei-revision").get(0);

    const value = getIdEncodedAttribute(teiRevisionElement, "tei-revision-id-");

    expect(value).toBe("pizza");
  });

  test("Returns null if the queried value was not encoded into the element's id attribute.", () => {
    const inputHtml = `<span class="tei-revision" id="tei-revision-dummy-pizza" />`;

    const $ = cheerio.load(inputHtml);

    const teiRevisionElement = $(".tei-revision").get(0);

    const value = getIdEncodedAttribute(teiRevisionElement, "tei-revision-id-");

    expect(value).toBe(null);
  });

  test("Returns null if the given HTML element has no id attribute", () => {
    const inputHtml = `<span class="tei-revision"/>`;

    const $ = cheerio.load(inputHtml);

    const teiRevisionElement = $(".tei-revision").get(0);

    const value = getIdEncodedAttribute(teiRevisionElement, "tei-revision-id-");

    expect(value).toBe(null);
  });
});

describe("getReason()", () => {
  test("returns error reason value encoded in class value.", () => {
    const inputHtml = `<span class="tei-error tei-error-xtype-cheese tei-error-eid-pizza tei-error-reason-because" />`;

    const $ = cheerio.load(inputHtml);

    const teiErrorElement = $(".tei-error").get(0);

    const reason = getReason(teiErrorElement);

    expect(reason).toBe("because");
  });

  test("returns expected value regardless of its position in the class values string", () => {
    /*
    Example, getReason() should return "pizza" for both of these HTML elements:

    <span class="tei-error tei-error-reason-pizza tei-error-eid-joke tei-error-xtype-something">
                                     ^
    <span class="tei-error tei-error-eid-joke tei-error-xtype-something tei-error-reason-pizza">
                                                                                  ^
    */
    const inputHtml = `<span class="tei-error tei-error-xtype-something tei-error-eid-joke tei-error-reason-pizza" />`;

    const $ = cheerio.load(inputHtml);

    const teiErrorElement = $(".tei-error").get(0);

    const reason = getReason(teiErrorElement);

    expect(reason).toBe("pizza");
  });
});

describe("getXType()", () => {
  test("returns xtype value encoded in class value.", () => {
    const inputHtml = `<span class="tei-error tei-error-xtype-idiom" />`;

    const $ = cheerio.load(inputHtml);

    const teiErrorElement = $(".tei-error").get(0);

    const xType = getXType(teiErrorElement);

    expect(xType).toBe("idiom");
  });

  test("returns expected value regardless of its position in the class values string", () => {
    /*
    Example, getXType() should return "pizza" for both of these HTML elements:

    <span class="tei-error tei-error-xtype-pizza tei-error-eid-joke">
                                     ^
    <span class="tei-error tei-error-eid-joke tei-error-xtype-pizza">
                                                        ^
    */
    const inputHtml = `<span class="tei-error tei-error-eid-joke tei-error-something tei-error-xtype-pizza" />`;

    const $ = cheerio.load(inputHtml);

    const teiErrorElement = $(".tei-error").get(0);

    const xType = getXType(teiErrorElement);

    expect(xType).toBe("pizza");
  });
});

describe("toHtml() throws errors on missing or empty inputXml argument.", () => {
  test("toHtml() throws InvalidInputError on missing argument.", () => {
    expect(() => {
      toHtml();
    }).toThrowError(InvalidInputError);
  });

  test("toHtml() throws error message 'Missing argument inputXml' on missing argument.", () => {
    expect(() => {
      toHtml();
    }).toThrowError("Missing argument inputXml.");
  });

  test("toHtml() throws InvalidInputError on empty string", () => {
    expect(() => {
      toHtml("");
    }).toThrowError(InvalidInputError);
  });

  test("toHtml() throws error message 'Received empty inputXml string.' on empty string", () => {
    expect(() => {
      toHtml("");
    }).toThrowError("Received empty inputXml string.");
  });
});

describe("toHtml() throws errors when inputXml is not empty but contains no word elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text><body><p n="1">
    <s n="1">
    </s>
    </p></body></text>
    </TEI>`;

  test("throws InvalidInputError.", () => {
    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });

  test("throws error message 'Input XML tree contains no word elements.'.", () => {
    expect(() => {
      toHtml(inputXml);
    }).toThrowError("Received XML tree contains no word elements.");
  });
});

describe("toHtml() throws errors when there are problems with the received XML's text element.", () => {
  test("Throws InvalidInputError when received XML tree contains no text element.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <body><p n="1">
      <s n="1">
      <w>Hi</w>
      </s>
      </p></body>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });

  test("Throws error message 'Received XML tree contains no text element.'.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <body><p n="1">
      <s n="1">
      <w>Hi</w>
      </s>
      </p></body>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError("Received XML tree contains no text element.");
  });

  test("Throws InvalidInputError when received XML tree contains more than one text element.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text><body><p n="1">
      <s n="1">
      <w>Hi</w>
      </s>
      </p></body></text><text></text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });

  test("Throws error message 'Received XML tree contains more than one text element.'.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text><body><p n="1">
      <s n="1">
      <w>Hi</w>
      </s>
      </p></body></text><text></text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError("Received XML tree contains more than one text element.");
  });
});

describe("toHtml() throws errors when there are problems with the received XML's body element.", () => {
  test("Throws InvalidInputError when received XML tree contains no body element even when word elements are present.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text><p n="1">
      <s n="1">
      <w>Hi</w>
      </s>
      </p></text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });

  test("Throws error message 'Received XML tree contains no body element.' when received XML tree contains no body element even when word elements are present.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>><p n="1">
      <s n="1">
      <w>Hi</w>
      </s>
      </p></text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError("Received XML tree contains no body element.");
  });

  test("Throws InvalidInputError when received XML tree contains more than one body element.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>
      <body><p n="1"><s n="1"><w>Hi</w></s></p></body>
      <body></body>
      </text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });

  test("Throws error message 'Received XML tree contains more than one body element.'.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>
      <body><p n="1"><s n="1"><w>Hi</w></s></p></body>
      <body></body>
      </text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError("Received XML tree contains more than one body element.");
  });

  test("Throws InvalidInputError when body element is not an immediate descendant of the text element.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text></text>
      <body><p n="1"><s n="1"><w>Hi</w></s></p></body>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });

  test("Throws error message 'Body element is not an immediate descendant of the text element.'.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text></text>
      <body><p n="1"><s n="1"><w>Hi</w></s></p></body>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(
      "Body element is not an immediate descendant of the text element."
    );
  });
});

describe("toHtml() throws errors when received XML already contains tei-* elements.", () => {
  /*
  TODO: Received XML cannot contain elements with the following classes:
    tei-?, Need to adapt for the revision related elements.
  */

  test("Throws InvalidInputError when received XML already contains elements with a class of 'tei-container'.", () => {
    const inputXml = `
      <span class="tei-container"></span>
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>
        <body>
          <p n="1">
            <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
            <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
          </p>
        </body>
      </text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });

  test("Throws InvalidInputError when received XML already contains elements with a class of 'tei-p'.", () => {
    const inputXml = `
      <span class="tei-p"></span>
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>
        <body>
          <p n="1">
            <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
            <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
          </p>
        </body>
      </text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });

  test("Throws InvalidInputError when received XML already contains elements with a class of 'tei-s'.", () => {
    const inputXml = `
      <span class="tei-s"></span>
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>
        <body>
          <p n="1">
            <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
            <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
          </p>
        </body>
      </text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });

  test("Throws InvalidInputError when received XML already contains elements with a class of 'tei-w'.", () => {
    const inputXml = `
      <span class="tei-w"></span>
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>
        <body>
          <p n="1">
            <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
            <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
          </p>
        </body>
      </text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });

  test("Throws InvalidInputError when received XML already contains elements with a class of 'tei-c'.", () => {
    const inputXml = `
      <span class="tei-c"></span>
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>
        <body>
          <p n="1">
            <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
            <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
          </p>
        </body>
      </text>
      </TEI>`;

    expect(() => {
      toHtml(inputXml);
    }).toThrowError(InvalidInputError);
  });
});

describe("toHtml() processes body elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Only one of the elements in the returned tree has a class of 'tei-container'.", () => {
    const totalTeiContainerElements = $(".tei-container").length;

    expect(totalTeiContainerElements).toBe(1);
  });

  test("tei-container has no parent node.", () => {
    const parentNode = $(".tei-container").get(0).parentNode;

    expect(parentNode).toBe(null);
  });

  test("There are no elements to the left of the tei-container.", () => {
    const teiContainerPrevSibling = $(".tei-container").get(0).previousSibling;

    expect(teiContainerPrevSibling).toBe(null);
  });

  test("There are no elements to the right of the tei-container.", () => {
    const teiContainerNextSibling = $(".tei-container").get(0).nextSibling;

    expect(teiContainerNextSibling).toBe(null);
  });

  test("The element with a class of 'tei-container' has a span tag.", () => {
    const tagName = $(".tei-container").get(0).tagName;

    expect(tagName).toBe("span");
  });
});

describe("toHtml() processes p elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1"><w>Goodbye</w><w>dear</w><c>.</c></s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Adds class of 'tei-p' to p elements.", () => {
    const totalElementsWithClassOfTeiP = $(".tei-p").length;

    expect(totalElementsWithClassOfTeiP).toBe(2);
  });

  test("Encodes 'n' attribute and value into HTML id.", () => {
    const p1 = $(".tei-p").get(0);
    const p2 = $(".tei-p").get(1);

    const p1Number = getIdEncodedAttribute(p1, "tei-p-n-");
    const p2Number = getIdEncodedAttribute(p2, "tei-p-n-");

    const success = p1Number === "1" && p2Number === "2";

    expect(success).toBe(true);
  });

  test("Removes 'n' attribute from elements with a class of 'tei-p'.", () => {
    const totalElementsWithClassOfTeiP = $(".tei-p").length;

    let foundTeiPElementWithNAttribute = false;

    let index;

    for (index = 0; index < totalElementsWithClassOfTeiP; index += 1) {
      if ($(".tei-p").get(index).attribs.n) {
        foundTeiPElementWithNAttribute = true;
        break;
      }
    }

    expect(foundTeiPElementWithNAttribute).toBe(false);
  });

  test("All elements with a class of 'tei-p' have a span tag.", () => {
    const totalElementsWithClassOfTeiP = $(".tei-p").length;

    let foundTeiPElementWithoutSpanTag = false;

    let index;

    for (index = 0; index < totalElementsWithClassOfTeiP; index += 1) {
      if ($(".tei-p").get(index).tagName !== "span") {
        foundTeiPElementWithoutSpanTag = true;
        break;
      }
    }

    expect(foundTeiPElementWithoutSpanTag).toBe(false);
  });
});

describe("toHtml() processes s elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1"><w>Goodbye</w><w>dear</w><c>.</c></s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Adds class of 'tei-s' to s elements.", () => {
    const totalElementsWithClassOfTeiS = $(".tei-s").length;

    expect(totalElementsWithClassOfTeiS).toBe(3);
  });

  test("Encodes 'n' attribute and value into HTML id.", () => {
    const s1 = $(".tei-s").get(0);
    const s2 = $(".tei-s").get(1);

    const s1Number = getIdEncodedAttribute(s1, "tei-s-n-");
    const s2Number = getIdEncodedAttribute(s2, "tei-s-n-");

    const success = s1Number === "1" && s2Number === "2";

    expect(success).toBe(true);
  });

  test("Removes 'n' attribute from elements with a class of 'tei-s'.", () => {
    const totalElementsWithClassOfTeiS = $(".tei-s").length;

    let foundTeiSElementWithNAttribute = false;

    let index;

    for (index = 0; index < totalElementsWithClassOfTeiS; index += 1) {
      if ($(".tei-s").get(index).attribs.n) {
        foundTeiSElementWithNAttribute = true;
        break;
      }
    }

    expect(foundTeiSElementWithNAttribute).toBe(false);
  });

  test("All elements with a class of 'tei-s' have a span tag.", () => {
    const totalElementsWithClassOfTeiS = $(".tei-s").length;

    let foundTeiSElementWithoutSpanTag = false;

    let index;

    for (index = 0; index < totalElementsWithClassOfTeiS; index += 1) {
      if ($(".tei-s").get(index).tagName !== "span") {
        foundTeiSElementWithoutSpanTag = true;
        break;
      }
    }

    expect(foundTeiSElementWithoutSpanTag).toBe(false);
  });
});

describe("toHtml() processes w elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1"><w>Goodbye</w><w>dear</w><c>.</c></s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Adds class of 'tei-w' to w elements.", () => {
    const totalElementsWithClassOfTeiW = $(".tei-w").length;

    expect(totalElementsWithClassOfTeiW).toBe(7);
  });

  test("All elements with a class of 'tei-w' have a span tag.", () => {
    const totalElementsWithClassOfTeiW = $(".tei-w").length;

    let foundTeiWElementWithoutSpanTag = false;

    let index;

    for (index = 0; index < totalElementsWithClassOfTeiW; index += 1) {
      if ($(".tei-w").get(index).tagName !== "span") {
        foundTeiWElementWithoutSpanTag = true;
        break;
      }
    }

    expect(foundTeiWElementWithoutSpanTag).toBe(false);
  });
});

describe("toHtml() processes c elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1"><w>Goodbye</w><w>dear</w><c>.</c></s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Adds class of 'tei-c' to c elements.", () => {
    const totalElementsWithClassOfTeiC = $(".tei-c").length;

    expect(totalElementsWithClassOfTeiC).toBe(3);
  });

  test("All elements with a class of 'tei-c' have a span tag.", () => {
    const totalElementsWithClassOfTeiC = $(".tei-c").length;

    let foundTeiCElementWithoutSpanTag = false;

    let index;

    for (index = 0; index < totalElementsWithClassOfTeiC; index += 1) {
      if ($(".tei-c").get(index).tagName !== "span") {
        foundTeiCElementWithoutSpanTag = true;
        break;
      }
    }

    expect(foundTeiCElementWithoutSpanTag).toBe(false);
  });
});

describe("toHtml() processes hi (italics) elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1"><w>Hello</w><w><hi>darling</hi></w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w><hi>you</hi></w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1"><w>Goodbye</w><w>dear</w><c>.</c></s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Adds class of 'tei-hi' to 'hi' elements.", () => {
    const totalElementsWithClassOfTeiHi = $(".tei-hi").length;

    expect(totalElementsWithClassOfTeiHi).toBe(2);
  });

  test("All elements with a class of 'tei-hi' have a span tag.", () => {
    const totalElementsWithClassOfTeiHi = $(".tei-hi").length;

    let foundTeiHiElementWithoutSpanTag = false;

    let index;

    for (index = 0; index < totalElementsWithClassOfTeiHi; index += 1) {
      if ($(".tei-hi").get(index).tagName !== "span") {
        foundTeiHiElementWithoutSpanTag = true;
        break;
      }
    }

    expect(foundTeiHiElementWithoutSpanTag).toBe(false);
  });
});

describe("toHtml() processes revision elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1">
            <revision id="5cat">
              <original><w>Jello</w></original>
              <corrected><w>Hello</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1">
            <revision id="1dog">
              <original><w>Goodbay</w></original>
              <corrected><w>Goodbye</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <revision id="3mouse">
              <original><w>Deer</w></original>
              <corrected><w>Dear</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <c>.</c>
          </s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Adds class of 'tei-revision' to revision elements.", () => {
    const totalElementsWithClassOfTeiRevision = $(".tei-revision").length;

    expect(totalElementsWithClassOfTeiRevision).toBe(3);
  });

  test("All elements with a class of 'tei-revision' have a span tag.", () => {
    const totalElementsWithClassOfTeiRevision = $(".tei-revision").length;

    let foundTeiRevisionElementWithoutSpanTag = false;

    let index;

    for (index = 0; index < totalElementsWithClassOfTeiRevision; index += 1) {
      if ($(".tei-revision").get(index).tagName !== "span") {
        foundTeiRevisionElementWithoutSpanTag = true;
        break;
      }
    }

    expect(foundTeiRevisionElementWithoutSpanTag).toBe(false);
  });

  test("Encodes revision ids using HTML id attribute.", () => {
    /*
    Elements with a class of tei-revision have their original revision id encoded
    as an HTML id of "tei-revision-id-x" where x is the original revision id. This
    rule should apply even if the original revision id was "1x" or whatever other
    alphanumerical string.
    */
    let matchedIds = [false, false, false];
    const expectedIds = [
      "tei-revision-id-5cat",
      "tei-revision-id-1dog",
      "tei-revision-id-3mouse",
    ];

    for (let index = 0; index < $(".tei-revision").length; index += 1) {
      if ($(".tei-revision").get(index).attribs.id === expectedIds[index]) {
        matchedIds[index] = true;
      }
    }

    const allIdsWherePreserved = matchedIds.every((item) => item);

    expect(allIdsWherePreserved).toBe(true);
  });
});

describe("toHtml() processes original elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1">
            <revision id="5cat">
              <original><w>Jello</w></original>
              <corrected><w>Hello</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1">
            <revision id="1dog">
              <original><w>Goodbay</w></original>
              <corrected><w>Goodbye</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <revision id="3mouse">
              <original><w>Deer</w></original>
              <corrected><w>Dear</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <c>.</c>
          </s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Adds class of 'tei-original' to original elements.", () => {
    const totalElementsWithClassOfTeiOriginal = $(".tei-original").length;

    expect(totalElementsWithClassOfTeiOriginal).toBe(3);
  });

  test("All elements with a class of 'tei-original' have a span tag.", () => {
    const totalElementsWithClassOfTeiOriginal = $(".tei-original").length;

    let foundTeiOriginalElementWithoutSpanTag = false;

    for (
      let index = 0;
      index < totalElementsWithClassOfTeiOriginal;
      index += 1
    ) {
      if ($(".tei-original").get(index).tagName !== "span") {
        foundTeiOriginalElementWithoutSpanTag = true;
        break;
      }
    }

    expect(foundTeiOriginalElementWithoutSpanTag).toBe(false);
  });
});

describe("toHtml() processes corrected elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1">
            <revision id="5cat">
              <original><w>Jello</w></original>
              <corrected><w>Hello</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1">
            <revision id="1dog">
              <original><w>Goodbay</w></original>
              <corrected><w>Goodbye</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <revision id="3mouse">
              <original><w>Deer</w></original>
              <corrected><w>Dear</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <c>.</c>
          </s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Adds class of 'tei-corrected' to corrected elements.", () => {
    const totalElementsWithClassOfTeiCorrected = $(".tei-corrected").length;

    expect(totalElementsWithClassOfTeiCorrected).toBe(3);
  });

  test("All elements with a class of 'tei-corrected' have a span tag.", () => {
    const totalElementsWithClassOfTeiCorrected = $(".tei-corrected").length;

    let foundTeiCorrectedElementWithoutSpanTag = false;

    for (
      let index = 0;
      index < totalElementsWithClassOfTeiCorrected;
      index += 1
    ) {
      if ($(".tei-corrected").get(index).tagName !== "span") {
        foundTeiCorrectedElementWithoutSpanTag = true;
        break;
      }
    }

    expect(foundTeiCorrectedElementWithoutSpanTag).toBe(false);
  });
});

describe("toHtml() processes 'errors' (plural) elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1">
            <revision id="5cat">
              <original><w>Jello</w></original>
              <corrected><w>Hello</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1">
            <revision id="1dog">
              <original><w>Goodbay</w></original>
              <corrected><w>Goodbye</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <revision id="3mouse">
              <original><w>Deer</w></original>
              <corrected><w>Dear</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <c>.</c>
          </s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Adds class of 'tei-errors' to errors elements.", () => {
    const totalElementsWithClassOfTeiErrors = $(".tei-errors").length;

    expect(totalElementsWithClassOfTeiErrors).toBe(3);
  });

  test("All elements with a class of 'tei-errors' have a span tag.", () => {
    const totalElementsWithClassOfTeiErrors = $(".tei-errors").length;

    let foundTeiErrorsElementWithoutSpanTag = false;

    for (let index = 0; index < totalElementsWithClassOfTeiErrors; index += 1) {
      if ($(".tei-errors").get(index).tagName !== "span") {
        foundTeiErrorsElementWithoutSpanTag = true;
        break;
      }
    }

    expect(foundTeiErrorsElementWithoutSpanTag).toBe(false);
  });
});

describe("toHtml() processes 'error' (singular) elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1">
            <revision id="5cat">
              <original><w>Jello</w></original>
              <corrected><w>Hello</w></corrected>
              <errors>
                <error xtype="magic" eid="cheese" reason="I" depId="just" idx="you"/>
                <error xtype="pizza" eid="is" reason="say" depId="trust" idx="really"/>
                <error xtype="pie" eid="good" reason="so" depId="me" idx="should"/>
              </errors> 
            </revision>
            <w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1">
            <revision id="1dog">
              <original><w>Goodbay</w></original>
              <corrected><w>Goodbye</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <revision id="3mouse">
              <original><w>Deer</w></original>
              <corrected><w>Dear</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors> 
            </revision>
            <c>.</c>
          </s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Adds class of 'tei-error' to error elements.", () => {
    const totalElementsWithClassOfTeiError = $(".tei-error").length;

    expect(totalElementsWithClassOfTeiError).toBe(5);
  });

  test("All elements with a class of 'tei-error' have a span tag.", () => {
    const totalElementsWithClassOfTeiError = $(".tei-error").length;

    let foundTeiErrorElementWithoutSpanTag = false;

    for (let index = 0; index < totalElementsWithClassOfTeiError; index += 1) {
      if ($(".tei-error").get(index).tagName !== "span") {
        foundTeiErrorElementWithoutSpanTag = true;
        break;
      }
    }

    expect(foundTeiErrorElementWithoutSpanTag).toBe(false);
  });

  test("xtype attribute and value get encoded into class attribute.", () => {
    /*
    Before:
      <error xtype="idiom" />
    
      <span class="tei-error tei-error-xtype-idiom" />
    */
    const xType1 = getXType($(".tei-error").get(0));
    const xType2 = getXType($(".tei-error").get(1));
    const xType3 = getXType($(".tei-error").get(2));

    const xTypesWereSuccessfullyEncoded =
      xType1 === "magic" && xType2 === "pizza" && xType3 === "pie";

    expect(xTypesWereSuccessfullyEncoded).toBe(true);
  });

  test("Removes 'xtype' attribute from all elements with a class of 'tei-error'.", () => {
    const totalElementsWithClassOfTeiError = $(".tei-error").length;

    let foundTeiErrorElementWithXTypeAttribute = false;

    for (let index = 0; index < totalElementsWithClassOfTeiError; index += 1) {
      if ($(".tei-error").get(index).attribs.xtype) {
        foundTeiErrorElementWithXTypeAttribute = true;
        break;
      }
    }

    expect(foundTeiErrorElementWithXTypeAttribute).toBe(false);
  });

  test("eid attribute and value get encoded into class value.", () => {
    /*
    Before:
      <error xtype="idiom" eid="example1"/>

      <span class="tei-error tei-error-xtype-idiom tei-error-eid-example1" />
    */
    const eId1 = getEId($(".tei-error").get(0));
    const eId2 = getEId($(".tei-error").get(1));
    const eId3 = getEId($(".tei-error").get(2));

    const eIdsWereSuccessfullyEncoded =
      eId1 === "cheese" && eId2 === "is" && eId3 === "good";

    expect(eIdsWereSuccessfullyEncoded).toBe(true);
  });

  test("Removes 'eid' attribute from all elements with a class of 'tei-error'.", () => {
    const totalElementsWithClassOfTeiError = $(".tei-error").length;

    let foundTeiErrorElementWithEIdAttribute = false;

    for (let index = 0; index < totalElementsWithClassOfTeiError; index += 1) {
      if ($(".tei-error").get(index).attribs.eid) {
        foundTeiErrorElementWithEIdAttribute = true;
        break;
      }
    }

    expect(foundTeiErrorElementWithEIdAttribute).toBe(false);
  });

  test("reason attribute and value get encoded into class value.", () => {
    /*
    Before:
      <error reason="because" eid="cheese" xtype="rocks"/>
    
      <span class="tei-error tei-error-reason-because tei-error-eid-cheese tei-error-xtype-rocks" />
    */
    const reason1 = getReason($(".tei-error").get(0));
    const reason2 = getReason($(".tei-error").get(1));
    const reason3 = getReason($(".tei-error").get(2));

    const reasonsWereSuccessfullyEncoded =
      reason1 === "I" && reason2 === "say" && reason3 === "so";

    expect(reasonsWereSuccessfullyEncoded).toBe(true);
  });

  test("Removes 'reason' attribute from all elements with a class of 'tei-error'.", () => {
    const totalElementsWithClassOfTeiError = $(".tei-error").length;

    let foundTeiErrorElementWithReasonAttribute = false;

    for (let index = 0; index < totalElementsWithClassOfTeiError; index += 1) {
      if ($(".tei-error").get(index).attribs.reason) {
        foundTeiErrorElementWithReasonAttribute = true;
        break;
      }
    }

    expect(foundTeiErrorElementWithReasonAttribute).toBe(false);
  });

  test("depId attribute and value get encoded into class value.", () => {
    /*
    Before:
      <error depId="just" reason="because" eid="cheese" xtype="rocks"/>

      <span class="tei-error tei-error-depId-just tei-error-reason-because tei-error-eid-cheese tei-error-xtype-rocks" />
    */
    const element1 = $(".tei-error").get(0);
    const element2 = $(".tei-error").get(1);
    const element3 = $(".tei-error").get(2);

    const depId1 = getClassEncodedAttribute(element1, "tei-error-depId-");
    const depId2 = getClassEncodedAttribute(element2, "tei-error-depId-");
    const depId3 = getClassEncodedAttribute(element3, "tei-error-depId-");

    const depIdsWereSuccessfullyEncoded =
      depId1 === "just" && depId2 === "trust" && depId3 === "me";

    expect(depIdsWereSuccessfullyEncoded).toBe(true);
  });

  test("Removes 'depId' attribute from all elements with a class of 'tei-error'.", () => {
    const totalElementsWithClassOfTeiError = $(".tei-error").length;

    let foundTeiErrorElementWithDepIdAttribute = false;

    for (let index = 0; index < totalElementsWithClassOfTeiError; index += 1) {
      if ($(".tei-error").get(index).attribs.depId) {
        foundTeiErrorElementWithDepIdAttribute = true;
        break;
      }
    }

    expect(foundTeiErrorElementWithDepIdAttribute).toBe(false);
  });

  test("idx attribute and value get encoded into class value.", () => {
    /*
    Before:
      <error depId="just" reason="because" eid="cheese" xtype="rocks" idx="yeah"/>

      <span class="tei-error tei-error-depId-just tei-error-reason-because tei-error-eid-cheese tei-error-xtype-rocks tei-error-idx-yeah" />
    */
    const element1 = $(".tei-error").get(0);
    const element2 = $(".tei-error").get(1);
    const element3 = $(".tei-error").get(2);

    const idx1 = getClassEncodedAttribute(element1, "tei-error-idx-");
    const idx2 = getClassEncodedAttribute(element2, "tei-error-idx-");
    const idx3 = getClassEncodedAttribute(element3, "tei-error-idx-");

    const idxsWereSuccessfullyEncoded =
      idx1 === "you" && idx2 === "really" && idx3 === "should";

    expect(idxsWereSuccessfullyEncoded).toBe(true);
  });

  test("Removes 'idx' attribute from all elements with a class of 'tei-error'.", () => {
    const totalElementsWithClassOfTeiError = $(".tei-error").length;

    let foundTeiErrorElementWithIdxAttribute = false;

    for (let index = 0; index < totalElementsWithClassOfTeiError; index += 1) {
      if ($(".tei-error").get(index).attribs.idx) {
        foundTeiErrorElementWithIdxAttribute = true;
        break;
      }
    }

    expect(foundTeiErrorElementWithIdxAttribute).toBe(false);
  });

  test("Does not encode the reason, depId, eid or idx attributes if they were not defined in the error XML element.", () => {
    /*
    Example:

      <error xtype="idiom">
  
    Should be converted to this:

      <span class="tei-error tei-error-xtype-idiom">

    *Not* this:

      <span class="tei-error tei-error-xtype-idiom tei-error-reason-undefined tei-error-depId-undefined" />
    
    or any other variation like "tei-error-reason-null" or "tei-error-reason-"

    */
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>
        <body>
          <p n="1">
            <s n="1">
              <revision id="5cat">
                <original><w>Jello</w></original>
                <corrected><w>Hello</w></corrected>
                <errors>
                  <error xtype="magic"/>
                </errors> 
              </revision>
            </s>
          </p>
        </body>
      </text>
      </TEI>`;

    const outputHtml = toHtml(inputXml).html;

    const $ = cheerio.load(outputHtml, { xmlMode: true });

    const classValuesString = $(".tei-error").get(0).attribs.class;
    const classValuesArray = classValuesString.split(" ");

    const correctlyEncoded =
      classValuesArray.length === 2 &&
      classValuesArray.includes("tei-error") &&
      classValuesArray.includes("tei-error-xtype-magic");

    expect(correctlyEncoded).toBe(true);
  });
});

describe("toHtml() removes meaningless whitespace between HTML elements.", () => {
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1"><w>Hello</w><w>darling</w><c>.</c></s>
          <s n="2"><w>I</w><w>love</w><w>you</w><c>.</c></s>
        </p>
        <p n="2">
          <s n="1"><w>Goodbye</w><w>dear</w><c>.</c></s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;

  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Output HTML contains no meaningless whitespace between HTML elements.", () => {
    $(".tei-space-character").remove();
    const outputHtmlMinusTeiSpaceCharacterElements = $.html();

    const meaninglessWhitespaceOccurences = (
      outputHtmlMinusTeiSpaceCharacterElements.match(/>\s+</g) || []
    ).length;

    expect(meaninglessWhitespaceOccurences).toBe(0);
  });
});

describe("toHtml() converts Icelandic characters to their HTML code equivalent.", () => {
  const icelandicCharacters = "áéíýóöúæþðÁÉÍÝÓÖÚǼÞÐ";

  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1"><w>${icelandicCharacters}</w></s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;
  const $ = cheerio.load(outputHtml, { xmlMode: true });

  test("Characters were successfully converted.", () => {
    const receivedText = $(".tei-w").html();
    const expectedText = `&#xE1;&#xE9;&#xED;&#xFD;&#xF3;&#xF6;&#xFA;&#xE6;&#xFE;&#xF0;&#xC1;&#xC9;&#xCD;&#xDD;&#xD3;&#xD6;&#xDA;&#x1FC;&#xDE;&#xD0;`;

    expect(receivedText).toBe(expectedText);
  });
});

describe("toHtml() inserts blank space character spans after tei-w and tei-c elements where appropriate.", () => {
  /*
  TODO:
  Refactor so each of the requirements are tested individually.

  - No spaces are added between a word and a punctuation mark.
  - The punctuation mark that terminates the whole text should not be followed by a blank space character.
  */
  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader></teiHeader>
    <text>
      <body>
        <p n="1">
          <s n="1"><w>Hello</w><w>dear</w><c>.</s><s n="2"></c><w>How</w><w>are</w><w>you</w><c>?</c></s>
        </p>
      </body>
    </text>
    </TEI>`;

  const outputHtml = toHtml(inputXml).html;
  const $ = cheerio.load(outputHtml, { xmlMode: true });
  const humanReadableText = $(".tei-s").text();

  test("Returns expected string.", () => {
    expect(humanReadableText).toBe("Hello dear. How are you?");
  });
});

describe("toHtml() inserts blanks space character spans after tei-w elements.", () => {
  test("After words followed by a word.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>
        <body>
          <p n="1">
            <s n="1"><w>Hello</w><w>dear</w></s>
          </p>
        </body>
      </text>
      </TEI>`;

    const outputHtml = toHtml(inputXml).html;
    const $ = cheerio.load(outputHtml, { xmlMode: true });
    const firstWord = $(".tei-w");

    const blankSpaceWasInserted = firstWord
      .next()
      .hasClass("tei-space-character");

    expect(blankSpaceWasInserted).toBe(true);
  });

  test("Not after words followed by a punctuation mark.", () => {
    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text>
        <body>
          <p n="1">
            <s n="1"><w>Hello</w><w>dear</w><c>.</c></s>
            <s n="2"><w>How</w><w>are</w><w>you</w><c>?</c></s>
          </p>
        </body>
      </text>
      </TEI>`;

    const outputHtml = toHtml(inputXml).html;
    const $ = cheerio.load(outputHtml, { xmlMode: true });
    const lastWordFirstSentence = $(".tei-w").get(1);
    const lastWordSecondSentence = $(".tei-w").get(4);

    const firstBlankSpaceWasInserted =
      lastWordFirstSentence.next.attribs.class === "tei-c";

    const secondBlankSpaceWasInserted =
      lastWordSecondSentence.next.attribs.class === "tei-c";

    const blankSpacesWereInserted =
      firstBlankSpaceWasInserted && secondBlankSpaceWasInserted;

    expect(blankSpacesWereInserted).toBe(true);
  });

  test("After words followed by a corrected word.", () => {
    /*
    Example

    Input XML fragment:

    <w>Hello</w>
    <revision id="1">
      <original>
        <w>deer</w>
      </original>
      <corrected>
        <w>dear</w>
      </corrected>
      <errors>
        <error xtype="idiom" eid="0" />
      </errors>
    </revision>
    <w>pizza</w>

    Output HTML fragment:

        <span class="tei-w">Hello</span>
    --> <span class="tei-space-character"> </span>
        <span class="tei-revision tei-revision-id-1">
          <span class="tei-original">
            <span class="tei-w">deer</span>
          </span>
          <span class="tei-corrected">
            <span class="tei-w">dear</span>
          </span>
          <span class="tei-errors">
            <span class="tei-error tei-error-xtype-idiom tei-error-eid-0" />
          </span>
        </span>
        <span class="tei-w">pizza</span>
    */

    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
        <teiHeader></teiHeader>
        <text><body><p n="1">
        <s n="1">
          <w>Hello</w>
          <revision id="1">
            <original><w>deer</w></original>
            <corrected><w>dear</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors>
          </revision>
          <w>pizza</w>
          <w>pie</w>
          <c>.</c>
        </s>
      </p></body></text>
      </TEI>`;
    const outputHtml = toHtml(inputXml).html;
    const $ = cheerio.load(outputHtml, { xmlMode: true });

    const blankSpaceWasInserted = $(".tei-w")
      .get(0)
      .next.attribs.class.includes("tei-space-character");

    expect(blankSpaceWasInserted).toBe(true);
  });

  test("After words followed by deleted superflous word and then another word.", () => {
    /*
    Example

    Original sentence:
      My personal opinion...
    Correction:
      My opinion...

    Input XML fragment:

    <w>My</w>
    <revision id="1">
      <original>
        <w>personal</w>
      </original>
      <corrected>
      </corrected>
      <errors>
        <error xtype="superflous-word" eid="0" />
      </errors>
    </revision>
    <w>opinion</w>

    Output HTML fragment:

        <span class="tei-w">My</span>
    --> <span class="tei-space-character"> </span>
        <span class="tei-revision tei-revision-id-1">
          <span class="tei-original">
            <span class="tei-w">personal</span>
          </span>
          <span class="tei-corrected" />
          <span class="tei-errors">
            <span class="tei-error tei-error-xtype-superfluous-word tei-error-eid-0" />
          </span>
        </span>
        <span class="tei-w">pizza</span>
    
    Notes:

    1. The actual xtype code for a superfluous word might change.

    2. The test would fail if the final sentence would be rendered as:
         Myopinion...
       where there's no space between "My" and "opinion".
    */

    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
        <teiHeader></teiHeader>
        <text><body><p n="1">
        <s n="1">
          <w>My</w>
          <revision id="1">
            <original><w>personal</w></original>
            <corrected></corrected>
              <errors>
                <error xtype="superfluous-word" eid="0" />
              </errors>
          </revision>
          <w>opinion</w>
          <c>.</c>
        </s>
      </p></body></text>
      </TEI>`;
    const outputHtml = toHtml(inputXml).html;
    const $ = cheerio.load(outputHtml, { xmlMode: true });

    const blankSpaceWasInserted = $(".tei-w")
      .get(0)
      .next.attribs.class.includes("tei-space-character");

    expect(blankSpaceWasInserted).toBe(true);
  });
});

describe("toHtml() inserts blank space character spans after tei-revision elements.", () => {
  test("If the revision ends in a corrected word and the element after the revision is also a word.", () => {
    /*
    Example

    Input XML fragment:

    <w>Hello</w>
    <revision id="1">
      <original>
        <w>deer</w>
      </original>
      <corrected>
        <w>dear</w>
      </corrected>
      <errors>
        <error xtype="idiom" eid="0" />
      </errors>
    </revision>
    <w>pizza</w>
    <c>.</c>

    Output HTML fragment:

        <span class="tei-w">Hello</span>
        <span class="tei-space-character"> </span>
        <span class="tei-revision tei-revision-id-1">
          <span class="tei-original">
            <span class="tei-w">deer</span>
          </span>
          <span class="tei-corrected">
            <span class="tei-w">dear</span>
          </span>
          <span class="tei-errors">
            <span class="tei-error tei-error-xtype-idiom tei-error-eid-0" />
          </span>
        </span>
    --> <span class="tei-space-character"> </span>
        <span class="tei-w">pizza</span>

    Rendered text:

      Without feature:
        Hello deerdearpizza.
              ----    ^
      With feature:
        Hello deerdear pizza.
              ----    ^
      
      The dashes simulate how the UI typically renders the original vs. corrected
      words.
    */

    const inputXml = `
      <?xml version="1.0" encoding="utf-8"?>
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
        <teiHeader></teiHeader>
        <text><body><p n="1">
        <s n="1">
          <w>Hello</w>
          <revision id="1">
            <original><w>deer</w></original>
            <corrected><w>dear</w></corrected>
              <errors>
                <error xtype="idiom" eid="0" />
              </errors>
          </revision>
          <w>pizza</w>
          <c>.</c>
        </s>
      </p></body></text>
      </TEI>`;
    const outputHtml = toHtml(inputXml).html;
    const $ = cheerio.load(outputHtml, { xmlMode: true });

    const blankSpaceWasInserted = $(".tei-revision")
      .next()
      .hasClass("tei-space-character");

    expect(blankSpaceWasInserted).toBe(true);
  });
});

describe("toHtml() - Output HTML string contains no self-closing tags.", () => {
  /*
  TODO:
  The problem is that the input XML strings contain self-closing tags like:

    <error xtype="pizza" eid="0"/>

  that must be converted to valid HTML5 tags like this:

    <span class="tei-error tei-error-xtype-pizza tei-error-eid-0"></span>

  But toHtml() is implemented using Cheerio. And Cheerio in XML mode will
  serialize all childless elements as self-closing tags like this:

    <span class="tei-error tei-error-xtype-pizza tei-error-eid-0"/>

  Unfortunately, valid HTML5 span elements must contain closing tags, even
  if they're childless. Otherwise span elements serialized as self-closing tags
  will cause rendering bugs in the browser.

  So toHtml() needs to sanitize its output and convert any elements with
  self-closing tags to elements with closing tags. That's a bit hard to test
  because ideally toHtml() should be able to encode XML attributes regardless
  of their position in the corresponding className strings. For example,

    <span class="tei-error tei-error-xtype-pizza tei-error-eid-0"></span>

  should be just as valid as:

    <span class="tei-error-eid-0 tei-error-xtype-pizza tei-error"></span>
  
  So I can either specify toHtml() so it always places the class encoded
  attributes in a pre-defined order and then check that toHtml() produces a
  deterministic serialized HTML string. Or for now I can just have a test that
  passes if the output does not contain the "/>" pattern.
  */

  const inputXml = `
    <?xml version="1.0" encoding="utf-8"?>
      <TEI xmlns="http://www.tei-c.org/ns/1.0">
      <teiHeader></teiHeader>
      <text><body><p n="1">
      <s n="1">
        <w>Hello</w>
        <revision id="1">
          <original><w>deer</w></original>
          <corrected><w>dear</w></corrected>
            <errors>
              <error xtype="idiom" eid="0" />
            </errors>
        </revision>
        <w>pizza</w>
        <c>.</c>
      </s>
    </p></body></text>
    </TEI>`;
  const outputHtml = toHtml(inputXml).html;

  test("Output contains no self-closing tags", () => {
    const containsSelfClosingTags = outputHtml.includes("/>");

    expect(containsSelfClosingTags).toBe(false);
  });
});

describe("toXml() returned string contains XML declaration.", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  const xmlDeclaration = $.root().get(0).children[0];

  const xmlDeclarationData = xmlDeclaration.data
    .replace(/\?/g, "")
    .replace(/'/g, `"`)
    .split(" ");

  test("First element's tag name equals '?xml'.", () => {
    const tagName = xmlDeclaration.name;

    expect(tagName).toBe("?xml");
  });

  test("XML declaration defines version 1.0", () => {
    expect(xmlDeclarationData.includes(`version="1.0"`)).toBe(true);
  });

  test("XML declaration defines utf-8 encoding", () => {
    expect(xmlDeclarationData.includes(`encoding="utf-8"`)).toBe(true);
  });
});

describe("toXml() returned string contains TEI element", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  const teiElement = $.root().get(0).children[1];

  test("Second element's tag is equal to TEI", () => {
    const tagName = teiElement.name;

    expect(tagName).toBe("TEI");
  });

  test("TEI element has expected namespace.", () => {
    const nameSpace = teiElement.attribs.xmlns;

    expect(nameSpace).toBe(`http://www.tei-c.org/ns/1.0`);
  });
});

describe("toXml() returned string contains teiHeader element", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  const teiElement = $.root().get(0).children[1];

  test("teiHeader element is TEI element's first child.", () => {
    const firstChildsTagName = teiElement.children[0].name;

    expect(firstChildsTagName).toBe("teiHeader");
  });
});

describe("toXml() returned string contains text element", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  const teiElement = $.root().get(0).children[1];

  test("text element is TEI element's second child.", () => {
    const secondChildsTagName = teiElement.children[1].name;

    expect(secondChildsTagName).toBe("text");
  });
});

describe("toXml() returned string contains body element", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  const textElement = $("text").get(0);

  test("body element is text element's first child.", () => {
    const firstChildsTagName = textElement.children[0].name;

    expect(firstChildsTagName).toBe("body");
  });
});

describe("toXml() input validation", () => {
  test("Throws error if given HTML tree contains '.tei-container' element.", () => {
    const inputHtml = `<span class="tei-container"><span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span></span>`;
    expect(() => {
      const outputXml = toXml(inputHtml);
    }).toThrowError(`Unexpected HTML element: ".tei-container"`);
  });
});

describe("toXml() processes '.tei-p' elements", () => {
  const inputHtml = `<span class="tei-p" id="tei-p-n-raspberry"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p" id="tei-p-n-pi"><span class="tei-s"><span class="tei-w">Bye</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output XML contains no '.tei-p' elements", () => {
    const totalElementsWithClassOfTeiP = $(".tei-p").length;

    expect(totalElementsWithClassOfTeiP).toBe(0);
  });

  test("Output contains as many 'p' elements as there were '.tei-p' elements in the given HTML plus one p element in the teiHeader", () => {
    const totalPElements = $("p").length;

    expect(totalPElements).toBe(3);
  });

  test("Paragraph number is correctly encoded.", () => {
    /*
    Warning:
    There's a p element in the teiHeader, but it doesn't represent a paragraph.
    */
    const firstParagraphNumber = $("p").get(1).attribs.n;
    const secondParagraphNumber = $("p").get(2).attribs.n;

    const successfulEncoding =
      firstParagraphNumber === "raspberry" && secondParagraphNumber === "pi";

    expect(successfulEncoding).toBe(true);
  });
});

describe("toXml() processes '.tei-s' elements", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s" id="tei-s-n-1"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s" id="tei-s-n-2"><span class="tei-w">Bye</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output XML contains no '.tei-s' elements", () => {
    const totalElementsWithClassOfTeiS = $(".tei-s").length;

    expect(totalElementsWithClassOfTeiS).toBe(0);
  });

  test("Output contains as many 's' elements as there were '.tei-s' elements in the given HTML", () => {
    const totalSElements = $("s").length;

    expect(totalSElements).toBe(2);
  });

  test("Sentence number is correctly encoded.", () => {
    const firstSentenceNumber = $("s").get(0).attribs.n;
    const secondSentenceNumber = $("s").get(1).attribs.n;

    const successfulEncoding =
      firstSentenceNumber === "1" && secondSentenceNumber === "2";

    expect(successfulEncoding).toBe(true);
  });
});

describe("toXml() processes '.tei-w' elements", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span class="tei-w">Bye</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output XML contains no '.tei-w' elements", () => {
    const totalElementsWithClassOfTeiW = $(".tei-w").length;

    expect(totalElementsWithClassOfTeiW).toBe(0);
  });

  test("Output contains as many 'w' elements as there were '.tei-w' elements in the given HTML", () => {
    const totalWElements = $("w").length;

    expect(totalWElements).toBe(5);
  });
});

describe("toXml() processes '.tei-c' elements", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span class="tei-w">Bye</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output XML contains no '.tei-c' elements", () => {
    const totalElementsWithClassOfTeiC = $(".tei-c").length;

    expect(totalElementsWithClassOfTeiC).toBe(0);
  });

  test("Output contains as many 'c' elements as there were '.tei-c' elements in the given HTML", () => {
    const totalCElements = $("c").length;

    expect(totalCElements).toBe(2);
  });
});

describe("toXml() processes '.tei-hi' elements", () => {
  const inputHtml =
    `<span class="tei-p">` +
    `<span class="tei-s">` +
    `<span class="tei-w">Hello</span>` +
    `<span class="tei-space-character"> </span>` +
    `<span id="tei-revision-id-1" class="tei-revision">` +
    `<span class="tei-original">` +
    `<span class="tei-w"><span class="tei-hi">deer</span></span>` + // tei-hi
    `</span>` +
    `<span class="tei-corrected">` +
    `<span class="tei-w"><span class="tei-hi">dear</span></span>` + // tei-hi
    `</span>` +
    `<span class="tei-errors">` +
    `<span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span>` +
    `</span>` +
    `</span>` +
    `<span class="tei-space-character"> </span>` +
    `<span class="tei-w">pizza</span>` +
    `<span class="tei-c">.</span>` +
    `</span>` +
    `</span>` +
    `<span class="tei-p">` +
    `<span class="tei-s">` +
    `<span class="tei-w">Bye</span>` +
    `<span class="tei-c">.</span>` +
    `</span>` +
    `</span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output XML contains no '.tei-hi' elements", () => {
    const totalElementsWithClassOfTeiHi = $(".tei-hi").length;

    expect(totalElementsWithClassOfTeiHi).toBe(0);
  });

  test("Output contains as many 'hi' elements as there were '.tei-hi' elements in the given HTML", () => {
    const totalHiElements = $("hi").length;

    expect(totalHiElements).toBe(2);
  });
});

describe("toXml() processes '.tei-space-character' elements", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span class="tei-w">Bye</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output contains no '.tei-space-character' elements.", () => {
    const totalTeiSpaceCharacterElements = $(".tei-space-character").length;

    expect(totalTeiSpaceCharacterElements).toBe(0);
  });
});

describe("toXml() processes '.tei-errors' elements", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span class="tei-w">Bye</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output contains no '.tei-errors' elements.", () => {
    const totalTeiErrorsElements = $(".tei-errors").length;

    expect(totalTeiErrorsElements).toBe(0);
  });

  test("Output contains as many 'errors' elements as there were '.tei-errors' elements in the given HTML", () => {
    const totalErrorsElements = $("errors").length;

    expect(totalErrorsElements).toBe(1);
  });
});

describe("toXml() processes '.tei-revision' elements", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-raspberry" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span id="tei-revision-id-pi" class="tei-revision"><span class="tei-original"><span class="tei-w">Bae</span></span><span class="tei-corrected"><span class="tei-w">Bye</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output contains no '.tei-revision' elements.", () => {
    const totalTeiRevisionElements = $(".tei-revision").length;

    expect(totalTeiRevisionElements).toBe(0);
  });

  test("Output contains as many 'revision' elements as there were '.tei-revision' elements in the given HTML", () => {
    const totalRevisionElements = $("revision").length;

    expect(totalRevisionElements).toBe(2);
  });

  test("Revision id is correctly encoded.", () => {
    const id1 = $("revision").get(0).attribs.id;
    const id2 = $("revision").get(1).attribs.id;

    const successfulEncoding = id1 === "raspberry" && id2 === "pi";

    expect(successfulEncoding).toBe(true);
  });
});

describe("toXml() processes '.tei-original' elements", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span class="tei-w">Bye</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output contains no '.tei-original' elements.", () => {
    const totalTeiOriginalElements = $(".tei-original").length;

    expect(totalTeiOriginalElements).toBe(0);
  });

  test("Output contains as many 'original' elements as there were '.tei-original' elements in the given HTML", () => {
    const totalOriginalElements = $("original").length;

    expect(totalOriginalElements).toBe(1);
  });
});

describe("toXml() processes '.tei-corrected' elements", () => {
  const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-1" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-idiom tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span class="tei-w">Bye</span><span class="tei-c">.</span></span></span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output contains no '.tei-corrected' elements.", () => {
    const totalTeiCorrectedElements = $(".tei-corrected").length;

    expect(totalTeiCorrectedElements).toBe(0);
  });

  test("Output contains as many 'corrected' elements as there were '.tei-corrected' elements in the given HTML", () => {
    const totalCorrectedElements = $("corrected").length;

    expect(totalCorrectedElements).toBe(1);
  });
});

describe("toXml() processes '.tei-error' (singular) elements", () => {
  const inputHtml =
    `<span class="tei-p">` +
    `<span class="tei-s">` +
    `<span class="tei-w">Hello</span>` +
    `<span class="tei-space-character"> </span>` +
    `<span id="tei-revision-id-raspberry" class="tei-revision">` +
    `<span class="tei-original">` +
    `<span class="tei-w">deer</span>` +
    `</span>` +
    `<span class="tei-corrected">` +
    `<span class="tei-w">dear</span>` +
    `</span>` +
    `<span class="tei-errors">` +
    `<span class="tei-error tei-error-xtype-content tei-error-reason-little tei-error-depId-why tei-error-eid-0 tei-error-idx-3"></span>` +
    `</span>` +
    `</span>` +
    `<span class="tei-space-character"> </span>` +
    `<span class="tei-w">pizza</span>` +
    `<span class="tei-c">.</span>` +
    `</span>` +
    `</span>` +
    `<span class="tei-p">` +
    `<span class="tei-s">` +
    `<span id="tei-revision-id-pi" class="tei-revision">` +
    `<span class="tei-original">` +
    `<span class="tei-w">Bae</span>` +
    `</span>` +
    `<span class="tei-corrected">` +
    `<span class="tei-w">Bye</span>` +
    `</span>` +
    `<span class="tei-errors">` +
    `<span class="tei-error tei-error-xtype-flaw tei-error-reason-cheese tei-error-depId-me tei-error-eid-1 tei-error-idx-4"></span>` +
    `</span>` +
    `</span>` +
    `<span class="tei-c">.</span>` +
    `</span>` +
    `</span>`;
  const outputXml = toXml(inputHtml);

  const $ = cheerio.load(outputXml, { xmlMode: true });

  test("Output contains no '.tei-error' elements.", () => {
    const totalTeiErrorElements = $(".tei-error").length;

    expect(totalTeiErrorElements).toBe(0);
  });

  test("Output contains as many 'error' elements as there were '.tei-error' elements in the given HTML", () => {
    const totalErrorElements = $("error").length;

    expect(totalErrorElements).toBe(2);
  });

  test("xtype attribute is correctly encoded", () => {
    const xtype1 = $("error").get(0).attribs.xtype;
    const xtype2 = $("error").get(1).attribs.xtype;

    const successfulEncoding = xtype1 === "content" && xtype2 === "flaw";

    expect(successfulEncoding).toBe(true);
  });

  test("Throws error if it finds '.tei-error' element with missing xtype attribute", () => {
    const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-raspberry" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-content tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span id="tei-revision-id-pi" class="tei-revision"><span class="tei-original"><span class="tei-w">Bae</span></span><span class="tei-corrected"><span class="tei-w">Bye</span></span><span class="tei-errors"><span class="tei-error tei-error-eid-1"></span></span></span><span class="tei-c">.</span></span></span>`;

    expect(() => {
      toXml(inputHtml);
    }).toThrowError("Found '.tei-error' element with a missing xtype");
  });

  test("reason attribute is correctly encoded", () => {
    const reason1 = $("error").get(0).attribs.reason;
    const reason2 = $("error").get(1).attribs.reason;

    const successfulEncoding = reason1 === "little" && reason2 === "cheese";

    expect(successfulEncoding).toBe(true);
  });

  test("Does not generate reason attribute if it was not encoded in the given HTML", () => {
    const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-raspberry" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-content tei-error-eid-0"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span id="tei-revision-id-pi" class="tei-revision"><span class="tei-original"><span class="tei-w">Bae</span></span><span class="tei-corrected"><span class="tei-w">Bye</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-flaw tei-error-eid-1"></span></span></span><span class="tei-c">.</span></span></span>`;
    const outputXml = toXml(inputHtml);

    const $ = cheerio.load(outputXml, { xmlMode: true });

    const reason1 = $("error").get(0).attribs.reason;
    const reason2 = $("error").get(1).attribs.reason;

    const success = !reason1 && !reason2;

    expect(success).toBe(true);
  });

  test("depId attribute is correctly encoded", () => {
    const depId1 = $("error").get(0).attribs.depId;
    const depId2 = $("error").get(1).attribs.depId;

    const successfulEncoding = depId1 === "why" && depId2 === "me";

    expect(successfulEncoding).toBe(true);
  });

  test("Does not generate depId attribute if it was not encoded in the given HTML", () => {
    const inputHtml =
      `<span class="tei-p">` +
      `<span class="tei-s">` +
      `<span class="tei-w">Hello</span><span class="tei-space-character"> </span>` +
      `<span id="tei-revision-id-raspberry" class="tei-revision">` +
      `<span class="tei-original"><span class="tei-w">deer</span></span>` +
      `<span class="tei-corrected"><span class="tei-w">dear</span></span>` +
      `<span class="tei-errors">` +
      `<span class="tei-error tei-error-xtype-content tei-error-eid-0"></span>` +
      `</span>` +
      `</span>` +
      `<span class="tei-space-character"> </span>` +
      `<span class="tei-w">pizza</span>` +
      `<span class="tei-c">.</span>` +
      `</span>` +
      `</span>` +
      `<span class="tei-p">` +
      `<span class="tei-s">` +
      `<span id="tei-revision-id-pi" class="tei-revision">` +
      `<span class="tei-original"><span class="tei-w">Bae</span></span>` +
      `<span class="tei-corrected"><span class="tei-w">Bye</span></span>` +
      `<span class="tei-errors">` +
      `<span class="tei-error tei-error-xtype-flaw tei-error-eid-1"></span>` +
      `</span></span><span class="tei-c">.</span>` +
      `</span>` +
      `</span>`;
    const outputXml = toXml(inputHtml);

    const $ = cheerio.load(outputXml, { xmlMode: true });

    const depId1 = $("error").get(0).attribs.depId;
    const depId2 = $("error").get(1).attribs.depId;

    const success = !depId1 && !depId2;

    expect(success).toBe(true);
  });

  test("eid attribute is correctly encoded", () => {
    const eid1 = $("error").get(0).attribs.eid;
    const eid2 = $("error").get(1).attribs.eid;

    const successfulEncoding = eid1 === "0" && eid2 === "1";

    expect(successfulEncoding).toBe(true);
  });

  test("Does not generate eid attribute if it was not encoded in the given HTML", () => {
    const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-raspberry" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-content"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span id="tei-revision-id-pi" class="tei-revision"><span class="tei-original"><span class="tei-w">Bae</span></span><span class="tei-corrected"><span class="tei-w">Bye</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-flaw"></span></span></span><span class="tei-c">.</span></span></span>`;
    const outputXml = toXml(inputHtml);

    const $ = cheerio.load(outputXml, { xmlMode: true });

    const eid1 = $("error").get(0).attribs.eid;
    const eid2 = $("error").get(1).attribs.eid;

    const success = !eid1 && !eid2;

    expect(success).toBe(true);
  });

  test("idx attribute is correctly encoded.", () => {
    const idx1 = $("error").get(0).attribs.idx;
    const idx2 = $("error").get(1).attribs.idx;

    const successfulEncoding = idx1 === "3" && idx2 === "4";

    expect(successfulEncoding).toBe(true);
  });

  test("Does not generate idx attribute if it was not encoded in the given HTML.", () => {
    const inputHtml = `<span class="tei-p"><span class="tei-s"><span class="tei-w">Hello</span><span class="tei-space-character"> </span><span id="tei-revision-id-raspberry" class="tei-revision"><span class="tei-original"><span class="tei-w">deer</span></span><span class="tei-corrected"><span class="tei-w">dear</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-content"></span></span></span><span class="tei-space-character"> </span><span class="tei-w">pizza</span><span class="tei-c">.</span></span></span><span class="tei-p"><span class="tei-s"><span id="tei-revision-id-pi" class="tei-revision"><span class="tei-original"><span class="tei-w">Bae</span></span><span class="tei-corrected"><span class="tei-w">Bye</span></span><span class="tei-errors"><span class="tei-error tei-error-xtype-flaw"></span></span></span><span class="tei-c">.</span></span></span>`;
    const outputXml = toXml(inputHtml);

    const $ = cheerio.load(outputXml, { xmlMode: true });

    const idx1 = $("error").get(0).attribs.idx;
    const idx2 = $("error").get(1).attribs.idx;

    const success = !idx1 && !idx2;

    expect(success).toBe(true);
  });
});

/*
TODO:

toHtml()
 - Encode idx attribute in error spans.

 - Validate features related to output object's teiHeader attributes

 - Make sure to correctly dea with abbreviations like "t.d." that should be
   encoded as one word. Don't treat these as encoding mistakes.

toXml()
 - Decode idx attribute in error spans.

 - Validate that p, s, w, c, revision, etc. elements don't contain an empty
   class attribute. I had to change the implementation to use
   removeAttr("class") instead of removeClass(). removeClass() will remove all
   class names but leave an empty class attribute. I didn't have time to add all
   the tests to validate this feature against future regressions.

 - Validate that p and s elements don't contain an id attribute.

 - Validate that revision elements and any other elements that can be navigated
   don't contain a style attribute. The frontend adds style attributes to
   navigatable elements so they can be highlighted.

 - Validate features related to optional teiHeader attribute and default value
   assignments. For example, what happens when these elements don't exist in
   original XML file?

 - Validate that the contents of the '.tei-w' elements were correctly encoded in
   the 'w' elements.

 - Validate that the contents of the '.tei-c' elements were correctly encoded in
   the 'c' elements.

 - Throws error on childless '.tei-w' element.

 - Throws error on childless '.tei-c' element.

 - Make sure to correctly dea with abbreviations like "t.d." that should be
   encoded as one word. Don't treat these as encoding mistakes.

 - None of the elements in the output XML tree contain a style attribute.

 - Throws an exception if there still exist span elements in the resulting XML
   tree.
*/
