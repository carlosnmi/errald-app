const { TestScheduler } = require("jest");
const {
  uiUpdateErrorsTable,
} = require("../public/js/uiUtils");

describe("uiUpdateErrorsTable()", () => {
  const table = document.createElement("table");
  const errors = [
    {
      xType: "idiom",
      reason: "style",
      depId: "5",
      eId: "17",
      idX: "34",
    },
    {
      xType: "superfluous",
      reason: null,
      depId: null,
      eId: null,
      idX: null,
    },
  ];

  uiUpdateErrorsTable(errors, table);

  test("Populates headers with the expected text", () => {
    const tableHeaderRowCells = table.children[0].children[0].children;

    const xTypeHeader = tableHeaderRowCells[0].textContent;
    const reasonHeader = tableHeaderRowCells[1].textContent;
    const depIdHeader = tableHeaderRowCells[2].textContent;
    const eIdHeader = tableHeaderRowCells[3].textContent;
    const idXHeader = tableHeaderRowCells[4].textContent;

    const success =
      xTypeHeader === "Type" &&
      reasonHeader === "Reason" &&
      depIdHeader === "Dep Id" &&
      eIdHeader === "Example Id" &&
      idXHeader === "idx";

    expect(success).toBe(true);
  });

  test(
    "Does not populate reason, dependency ID, example ID or idx cells when " +
      "given null values.",
    () => {
      const tableBodyRow2Cells = table.children[1].children[1].children;

      const reason = tableBodyRow2Cells[1].textContent;
      const depId = tableBodyRow2Cells[2].textContent;
      const eId = tableBodyRow2Cells[3].textContent;
      const idX = tableBodyRow2Cells[4].textContent;

      const success = !reason && !depId && !eId && !idX;

      expect(success).toBe(true);
    }
  );

  test("Populates body rows with the expected text", () => {
    const row1Cells = table.children[1].children[0].children;
    const row2Cells = table.children[1].children[1].children;

    const xTypeRow1 = row1Cells[0].textContent;
    const reasonRow1 = row1Cells[1].textContent;
    const depIdRow1 = row1Cells[2].textContent;
    const eIdRow1 = row1Cells[3].textContent;
    const idXRow1 = row1Cells[4].textContent;

    const xTypeRow2 = row2Cells[0].textContent;
    const reasonRow2 = row2Cells[1].textContent;
    const depIdRow2 = row2Cells[2].textContent;
    const eIdRow2 = row2Cells[3].textContent;
    const idXRow2 = row2Cells[4].textContent;

    const row1AsExpected =
      xTypeRow1 === "idiom" &&
      reasonRow1 === "style" &&
      depIdRow1 === "5" &&
      eIdRow1 === "17" &&
      idXRow1 === "34";
    const row2AsExpected =
      xTypeRow2 === "superfluous" &&
      !reasonRow2 &&
      !depIdRow2 &&
      !eIdRow2 &&
      !idXRow2;
    const success = row1AsExpected && row2AsExpected;

    expect(success).toBe(true);
  });

  test("Generates as many table body rows as given items in errors array", () => {
    const totalRows = table.children[1].children.length;

    expect(totalRows).toBe(2);
  });

  test("Removes old table entries", () => {
    const table = document.createElement("table");
    table.innerHTML =
      `<thead>` +
      `<tr><th>Type</th><th>Reason</th><th>Dep Id</th><th>Example Id</th><th>idx</th><th></th></tr>` +
      `</thead>` +
      `<tbody>` +
      `<tr><td>Hello</td><td>my</td><td>dear</td><td>pizza</td><td>pie</td></tr>` +
      `</tbody>`;

    const errors = [
      {
        xType: "idiom",
        reason: "style",
        depId: "5",
        eId: "17",
        idX: "34",
      },
    ];

    uiUpdateErrorsTable(errors, table);

    const row1 = table.children[1].children[0];

    const xTypeRow1 = row1.children[0].textContent;
    const reasonRow1 = row1.children[1].textContent;
    const depIdRow1 = row1.children[2].textContent;
    const eIdRow1 = row1.children[3].textContent;
    const idXRow1 = row1.children[4].textContent;

    const row1AsExpected =
      xTypeRow1 === "idiom" &&
      reasonRow1 === "style" &&
      depIdRow1 === "5" &&
      eIdRow1 === "17" &&
      idXRow1 === "34";

    expect(row1AsExpected).toBe(true);
  });
});
