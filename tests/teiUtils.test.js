const {
  appendErrorSpan,
  createRevision,
  createWordElement,
  getClassEncodedAttribute,
  getIdEncodedAttribute,
  getOriginalAndCorrectedText,
  getRevisionInfo,
  isValidXTypeInput,
  removeRevision,
  replaceElements,
} = require("../public/js/teiUtils");

const { TestScheduler } = require("jest");

describe("createWordElement()", () => {
  const word = createWordElement("hi");

  test("Returned element has a tagName of SPAN", () => {
    expect(word.tagName).toBe("SPAN");
  });

  test("Returned element has a className of tei-w", () => {
    expect(word.className).toBe("tei-w");
  });

  test("Returned element has given text content", () => {
    expect(word.textContent).toBe("hi");
  });

  test("Throws an error if passed more than one word", () => {
    expect(() => {
      const word = createWordElement("hello world");
    }).toThrowError("Received more than one word");
  });

  /*
  TODO:
  Add test to check if it throws an error when receiving a punctuation mark.
  */
});

describe("appendErrorSpan()", () => {
  test(`Appends the encoded error to the given ".tei-revision" span.`, () => {
    const teiRevision = document.createElement("span");
    const teiOriginal = document.createElement("span");
    const teiCorrected = document.createElement("span");
    const teiErrors = document.createElement("span");

    teiRevision.appendChild(teiOriginal);
    teiRevision.appendChild(teiCorrected);
    teiRevision.appendChild(teiErrors);

    appendErrorSpan(
      {
        xType: "cheese",
        reason: "pizza",
        depId: "is",
        eId: "awesome",
        idX: "possum",
      },
      teiRevision
    );

    const teiError = teiErrors.children[0];

    const xType = getClassEncodedAttribute(teiError, "tei-error-xtype-");
    const reason = getClassEncodedAttribute(teiError, "tei-error-reason-");
    const depId = getClassEncodedAttribute(teiError, "tei-error-depId-");
    const eId = getClassEncodedAttribute(teiError, "tei-error-eid-");
    const idX = getClassEncodedAttribute(teiError, "tei-error-idx-");

    const success =
      xType === "cheese" &&
      reason === "pizza" &&
      depId === "is" &&
      eId === "awesome" &&
      idX === "possum";

    expect(success).toBe(true);
  });

  test("Throws error when given xtype is an empty string, null or undefined.", () => {
    expect(() => {
      appendErrorSpan(
        { xType: null, reason: "pizza", depId: "is", eId: "awesome" },
        null
      );
    }).toThrowError("xtype must be a non-empty string");
  });

  test("Throws error when given revision span is null or undefined.", () => {
    expect(() => {
      appendErrorSpan(
        { xType: "cheese", reason: "pizza", depId: "is", eId: "awesome" },
        null
      );
    }).toThrowError("Revision is null or undefined");
  });

  test(`Throws error when given revision span contains no children.`, () => {
    const teiRevision = document.createElement("span");

    expect(() => {
      appendErrorSpan(
        { xType: "cheese", reason: "pizza", depId: "is", eId: "awesome" },
        teiRevision
      );
    }).toThrowError("Revision is childless");
  });

  test(
    "Does not append tei-error-* class names for reason, depId, eid or idx if " +
      "these values are null.",
    () => {
      const revisionSpan = document.createElement("span");
      revisionSpan.innerHTML =
        `<span class="tei-original">` +
        `<span class="tei-w">deer</span>` +
        `</span>` +
        `<span class="tei-corrected">` +
        `<span class="tei-w">dear</span>` +
        `</span>` +
        `<span class="tei-errors"></span>`;

      appendErrorSpan(
        { xType: "cheese", reason: null, depId: null, eId: null, idX: null },
        revisionSpan
      );

      const errorSpan = revisionSpan.children[2].children[0];

      const containsReasonClass = errorSpan.className.includes("reason");
      const containsDepIdClass = errorSpan.className.includes("depId");
      const containsEIdClass = errorSpan.className.includes("eid");
      const containsIdXClass = errorSpan.className.includes("idx");

      const success =
        !containsReasonClass &&
        !containsDepIdClass &&
        !containsEIdClass &&
        !containsIdXClass;

      expect(success).toBe(true);
    }
  );
});

describe("getClassEncodedAttribute()", () => {
  const teiError = document.createElement("span");
  teiError.className = "tei-error tei-error-eid-0 tei-error-xtype-pizza";

  test("Returns expected value if encoded in the given element.", () => {
    const value = getClassEncodedAttribute(teiError, "tei-error-xtype-");

    expect(value).toBe("pizza");
  });

  test("Returns null if the attribute was not encoded in the given element.", () => {
    const value = getClassEncodedAttribute(teiError, "tei-error-cheese-");

    expect(value).toBe(null);
  });
});

describe("getIdEncodedAttribute", () => {
  const teiRevision = document.createElement("span");
  teiRevision.id = "tei-revision-id-5";

  test("Returns expected value if attribute is encoded in the given element.", () => {
    const value = getIdEncodedAttribute(teiRevision, "tei-revision-id-");

    expect(value).toBe("5");
  });

  test("Returns null if the attribute was not encoded in the given element.", () => {
    const value = getIdEncodedAttribute(teiRevision, "tei-error-cheese-");

    expect(value).toBe(null);
  });
});

describe("getOriginalAndCorrectedText()", () => {
  test(
    "Returns expected value when the original and corrected spans contain each" +
      " a single word element.",
    () => {
      const teiRevision = document.createElement("span");
      teiRevision.className = "tei-revision";

      const teiOriginal = document.createElement("span");
      teiOriginal.className = "tei-original";

      const teiCorrected = document.createElement("span");
      teiCorrected.className = "tei-corrected";

      const originalWord = createWordElement("deer");

      const correctedWord = createWordElement("dear");

      teiOriginal.appendChild(originalWord);
      teiCorrected.appendChild(correctedWord);

      teiRevision.appendChild(teiOriginal);
      teiRevision.appendChild(teiCorrected);

      const result = getOriginalAndCorrectedText(teiRevision);
      const returnsExpectedResult =
        result.original === "deer" && result.corrected === "dear";

      expect(returnsExpectedResult).toBe(true);
    }
  );

  test("Returns null for the original text when the original span is childless", () => {
    const teiRevision = document.createElement("span");
    teiRevision.className = "tei-revision";

    const teiOriginal = document.createElement("span");
    teiOriginal.className = "tei-original";

    const teiCorrected = document.createElement("span");
    teiCorrected.className = "tei-corrected";

    const correctedWord = createWordElement("dear");

    teiCorrected.appendChild(correctedWord);

    teiRevision.appendChild(teiOriginal);
    teiRevision.appendChild(teiCorrected);

    const result = getOriginalAndCorrectedText(teiRevision);
    const returnsExpectedResult =
      result.original === null && result.corrected === "dear";

    expect(returnsExpectedResult).toBe(true);
  });

  test("Returns null for the corrected text when the corrected span is childless", () => {
    const teiRevision = document.createElement("span");
    teiRevision.className = "tei-revision";

    const teiOriginal = document.createElement("span");
    teiOriginal.className = "tei-original";

    const teiCorrected = document.createElement("span");
    teiCorrected.className = "tei-corrected";

    const originalWord = createWordElement("deer");

    teiOriginal.appendChild(originalWord);

    teiRevision.appendChild(teiOriginal);
    teiRevision.appendChild(teiCorrected);

    const result = getOriginalAndCorrectedText(teiRevision);
    const returnsExpectedResult =
      result.original === "deer" && result.corrected === null;

    expect(returnsExpectedResult).toBe(true);
  });

  test("Returns expected string when the original span contains more than one child.", () => {
    const teiRevision = document.createElement("span");
    teiRevision.className = "tei-revision";

    const teiOriginal = document.createElement("span");
    teiOriginal.className = "tei-original";

    const teiCorrected = document.createElement("span");
    teiCorrected.className = "tei-corrected";

    const originalWord1 = createWordElement("deer");
    const originalWord2 = createWordElement("peetsa");

    const blankSpaceSpan1 = document.createElement("span");
    blankSpaceSpan1.className = "tei-space-character";
    blankSpaceSpan1.textContent = " ";

    const correctedWord1 = createWordElement("dear");
    const correctedWord2 = createWordElement("pizza");

    const blankSpaceSpan2 = document.createElement("span");
    blankSpaceSpan2.className = "tei-space-character";
    blankSpaceSpan2.textContent = " ";

    teiOriginal.appendChild(originalWord1);
    teiOriginal.appendChild(blankSpaceSpan1);
    teiOriginal.appendChild(originalWord2);

    teiCorrected.appendChild(correctedWord1);
    teiCorrected.appendChild(blankSpaceSpan2);
    teiCorrected.appendChild(correctedWord2);

    teiRevision.appendChild(teiOriginal);
    teiRevision.appendChild(teiCorrected);

    const result = getOriginalAndCorrectedText(teiRevision);

    const returnsExpectedResult =
      result.original === "deer peetsa" && result.corrected === "dear pizza";

    expect(returnsExpectedResult).toBe(true);
  });
  /*
  TODO:

  Test the case when the original and corrected text contain empty word or
  punctuation elements (or any other invalid content for that matter).
  */
});

describe("getRevisionInfo()", () => {
  const revision = document.createElement("span");
  revision.id = "tei-revision-id-5";
  revision.innerHTML =
    `<span class="tei-original">` +
    `<span class="tei-w">deer</span>` +
    `</span>` +
    `<span class="tei-corrected">` +
    `<span class="tei-w">dear</span>` +
    `</span>` +
    `<span class="tei-errors">` +
    `<span class="tei-error tei-error-xtype-idiom tei-error-reason-style tei-error-depId-5 tei-error-eid-17 tei-error-idx-34"></span>` +
    `<span class="tei-error tei-error-xtype-superfluous tei-error-eid-0 tei-error-idx-40"></span>` +
    `</span>`;

  const sentence = document.createElement("span");
  sentence.className = "tei-s";
  sentence.id = "tei-s-n-4";

  sentence.appendChild(revision);

  const paragraph = document.createElement("span");
  paragraph.className = "tei-p";
  paragraph.id = "tei-p-n-3";

  paragraph.appendChild(sentence);

  const received = getRevisionInfo(revision);

  test("Returns expected revision id.", () => {
    expect(received.id).toBe("5");
  });

  test("Returns expected paragraph number.", () => {
    expect(received.paragraphNumber).toBe("3");
  });

  test("Returns expected sentence number.", () => {
    expect(received.sentenceNumber).toBe("4");
  });

  test("Returns expected original text.", () => {
    expect(received.originalText).toBe("deer");
  });

  test("Returns expected corrected text.", () => {
    expect(received.correctedText).toBe("dear");
  });

  test("Returns expected number of errors.", () => {
    expect(received.errors.length).toBe(2);
  });

  test("Returns expected error type values.", () => {
    const error1XType = received.errors[0].xType;
    const error2XType = received.errors[1].xType;

    const success = error1XType === "idiom" && error2XType === "superfluous";

    expect(success).toBe(true);
  });

  test("Returns expected error reason values.", () => {
    const error1Reason = received.errors[0].reason;
    const error2Reason = received.errors[1].reason;

    const success = error1Reason === "style" && !error2Reason;

    expect(success).toBe(true);
  });

  test("Returns expected revision dependency id values.", () => {
    const error1DepId = received.errors[0].depId;
    const error2DepId = received.errors[1].depId;

    const success = error1DepId === "5" && !error2DepId;

    expect(success).toBe(true);
  });

  test("Returns expected example id values.", () => {
    const error1EId = received.errors[0].eId;
    const error2EId = received.errors[1].eId;

    const success = error1EId === "17" && error2EId === "0";

    expect(success).toBe(true);
  });

  test("Returns expected idx values.", () => {
    const error1IdX = received.errors[0].idX;
    const error2IdX = received.errors[1].idX;

    const success = error1IdX === "34" && error2IdX === "40";

    expect(success).toBe(true);
  });
});

describe("isValidXTypeInput()", () => {
  const errorCodes = { cheese: null, pizza: null, lovely: null };

  test("Returns true if the given input is included in the given error codes.", () => {
    const result = isValidXTypeInput("cheese", errorCodes);

    expect(result).toBe(true);
  });

  test("Returns false if the given input is not included in the given error codes.", () => {
    const result = isValidXTypeInput("veggie", errorCodes);

    expect(result).toBe(false);
  });
});

describe("removeRevision()", () => {
  const sentenceSpan = document.createElement("span");
  sentenceSpan.innerHTML =
    `<span class="tei-revision">` +
    `<span class="tei-original">` +
    `<span class="tei-w">Naes</span>` +
    `<span class="tei-space-character"> </span>` +
    `<span class="tei-w">pitsa</span>` +
    `</span>` +
    `</span>` +
    `<span class="tei-w">pie</span><span class="tei-c">.</span>`;

  const revisionSpan = sentenceSpan.firstElementChild;

  removeRevision(revisionSpan);

  test("Inserts '.tei-original' span's contents before '.tei-revision' span", () => {
    const sentenceChild0 = sentenceSpan.children[0];
    const sentenceChild1 = sentenceSpan.children[1];
    const sentenceChild2 = sentenceSpan.children[2];

    const tagsAsExpected =
      sentenceChild0.tagName.toLowerCase() === "span" &&
      sentenceChild0.tagName.toLowerCase() === "span" &&
      sentenceChild0.tagName.toLowerCase() === "span";

    const classNamesAsExpected =
      sentenceChild0.classList.contains("tei-w") &&
      sentenceChild1.classList.contains("tei-space-character") &&
      sentenceChild2.classList.contains("tei-w");

    const textContentsAsExpected =
      sentenceChild0.textContent === "Naes" &&
      sentenceChild1.textContent === " " &&
      sentenceChild2.textContent === "pitsa";

    const success =
      tagsAsExpected && classNamesAsExpected && textContentsAsExpected;

    expect(success).toBe(true);
  });

  test("Removes '.tei-revision' span from enclosing 'tei-s' span.", () => {
    const success = sentenceSpan.contains(revisionSpan);

    expect(success).toBe(false);
  });

  test(
    "Adds '.selectable-token' class to all of the 'tei-original' elements " +
    "that have now replaced the revision.",
    () => {
      const selectableTokens = [
        sentenceSpan.children[0],
        sentenceSpan.children[2],
      ];

      const success = selectableTokens.every((elem) =>
        elem.classList.contains("selectable-token")
      );

      expect(success).toBe(true);
    }
  );

  /*
  TODO:
  Test that it can deal with revisions where the original and corrected spans
  positions have been swapped. That is, where corrected with be found within
  revisionSpan.children[0] rather than the expected children[1] position.
  */
});

describe("createRevision()", () => {
  const sentenceSpan = document.createElement("span");
  sentenceSpan.innerHTML =
    `<span class="tei-w selectable-token">Hello</span>` +
    `<span class="tei-w selectable-token selected-token">me</span>` +
    `<span class="tei-w selectable-token selected-token">derling</span>` +
    `<span class="tei-w selectable-token selected-token">sunshite</span>` +
    `<span class="tei-c selectable-token selected-token">.</span>` +
    `<span class="tei-w selectable-token selected-token">Pitsa</span>` +
    `<span class="tei-w selectable-token selected-token">pae</span>` +
    `<span class="tei-c selectable-token">.</span>` +
    `</span>`;
  
  const selectedTokens = sentenceSpan.querySelectorAll(".selected-token");
  
  const revision = createRevision("5a", selectedTokens);

  test("Returned element is a span.", () => {
    expect(revision.tagName.toLowerCase() === "span").toBe(true);
  });

  test("Returned element contains a class of 'tei-revision'.", () => {
    expect(revision.classList.contains("tei-revision")).toBe(true);
  });

  test("Returned element contains a class of 'selectable-token'.", () => {
    expect(revision.classList.contains("selectable-token")).toBe(true);
  });

  test("Returned element has the expected id.", () => {
    expect(revision.id === "tei-revision-id-5a").toBe(true);
  });

  test("Returned element's first child is a span with a class of 'tei-original'.", () => {
    const firstChildsTagName = revision.firstElementChild.tagName.toLowerCase();
    const firstChildsClassName = revision.firstElementChild.className;

    const success =
      firstChildsTagName === "span" && firstChildsClassName === "tei-original";

    expect(success).toBe(true);
  });

  test("Returned element's second child is a span with a class of 'tei-corrected'.", () => {
    const secondChildsTagName = revision.children[1].tagName.toLowerCase();
    const secondChildsClassName = revision.children[1].className;

    const success =
      secondChildsTagName === "span" &&
      secondChildsClassName === "tei-corrected";

    expect(success).toBe(true);
  });

  test("Returned element's third child is a span with a class of 'tei-errors'.", () => {
    const thirdChildsTagName = revision.children[2].tagName.toLowerCase();
    const thirdChildsClassName = revision.children[2].className;

    const success =
      thirdChildsTagName === "span" && thirdChildsClassName === "tei-errors";

    expect(success).toBe(true);
  });

  test(
    "The resulting '.tei-original' span contains as many '.tei-w' and " +
    "'.tei-c' elements as in the given NodeList",
    () => {
      const originalSpan = revision.querySelector(".tei-original");

      expect(originalSpan.querySelectorAll(".tei-w, .tei-c").length).toBe(6);
  });

  test("None of the resulting descendants has a class of 'selectable-token'.", () => {
    const revisionDescendants = Array.from(revision.querySelectorAll("span"));
    const success = revisionDescendants.every(
      (elem) => !elem.classList.contains("selectable-token")
    );

    expect(success).toBe(true);
  });

  test("None of the resulting descendants has a class of 'selected-token'.", () => {
    const revisionDescendants = Array.from(revision.querySelectorAll("span"));
    const success = revisionDescendants.every(
      (elem) => !elem.classList.contains("selected-token")
    );

    expect(success).toBe(true);
  });

  test("The resulting '.tei-original' element's children are all spans.", () => {
    const originalSpan = revision.querySelector(".tei-original");
    const originalChildren = Array.from(originalSpan.children);

    const allSpans = originalChildren.every(
      (child) => child.tagName.toLowerCase() === "span"
    );

    expect(allSpans).toBe(true);
  });

  test("The resulting '.tei-original' element's children contain the expected classes.", () => {
    const originalChildren = revision.querySelector(".tei-original").children;

    const success =
      originalChildren[0].classList.contains("tei-w") &&
      originalChildren[1].classList.contains("tei-space-character") &&
      originalChildren[2].classList.contains("tei-w") &&
      originalChildren[3].classList.contains("tei-space-character") &&
      originalChildren[4].classList.contains("tei-w") &&
      originalChildren[5].classList.contains("tei-c") &&
      originalChildren[6].classList.contains("tei-space-character") &&
      originalChildren[7].classList.contains("tei-w") &&
      originalChildren[8].classList.contains("tei-space-character") &&
      originalChildren[9].classList.contains("tei-w");

    expect(success).toBe(true);
  });

  test("The resulting '.tei-original' element's children contain the expected textContent.", () => {
    const originalChildren = revision.querySelector(".tei-original").children;

    const success =
      originalChildren[0].textContent === "me" &&
      originalChildren[1].textContent === " " &&
      originalChildren[2].textContent === "derling" &&
      originalChildren[3].textContent === " " &&
      originalChildren[4].textContent === "sunshite" &&
      originalChildren[5].textContent === "." &&
      originalChildren[6].textContent === " " &&
      originalChildren[7].textContent === "Pitsa" &&
      originalChildren[8].textContent === " " &&
      originalChildren[9].textContent === "pae";

    expect(success).toBe(true);
  });

  test("The '.tei-original' element's last child element does not have a class of 'tei-space-character'.", () => {
    const originalChildren = revision.querySelector(".tei-original").children;
    const lastChild = originalChildren[originalChildren.length - 1];

    expect(lastChild.classList.contains("tei-space-character")).toBe(false);
  });
});

describe("replaceElements()", () => {
  const elementsFactory = () => {
    const sentenceSpan = document.createElement("span");
    sentenceSpan.innerHTML =
      `<span class="tei-w selectable-token">Hello</span>` +
      `<span class="tei-w selectable-token selected-token to-remove">me</span>` +
      `<span class="tei-w selectable-token selected-token to-remove">derling</span>` +
      `<span class="tei-w selectable-token selected-token to-remove">sunshite</span>` +
      `<span class="tei-c selectable-token">.</span>` +
      `</span>`;

    const oldElements = sentenceSpan.querySelectorAll(".to-remove");
    const newElement = document.createElement("div");

    return { oldElements, newElement };
  };

  test("Removes the given old elements from their parent element.", () => {
    const { oldElements, newElement } = elementsFactory();
    const parentElement = oldElements[0].parentElement;

    replaceElements(oldElements, newElement);

    const potentialOldElements = parentElement.querySelectorAll(".to-remove");

    expect(potentialOldElements.length).toBe(0);
  });

  test("The new element has been appended to the old element's parent at the expected position.", () => {
    const { oldElements, newElement } = elementsFactory();
    const parentElement = oldElements[0].parentElement;

    replaceElements(oldElements, newElement);

    expect(parentElement.children[1].tagName.toLowerCase()).toBe("div");
  });
});

/*
TODO:
 - Test uiRemoveErrorSpan(), it was written with testability in mind.
 - Work on reassignRevisionId

describe("reassignRevisionId");
*/
