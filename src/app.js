#!/usr/bin/env node

const fs = require("fs");
const path = require("path");

const express = require("express");
const hbs = require("hbs");

const {
  toHtml,
  toXml,
  xmlTokensToHtml,
  InvalidInputError,
} = require("./teiParser");
const { beautifyXml } = require("./utils");
const { updateErrorCodes } = require("./errorCodeUtils");

const app = express();
const port = process.env.PORT || 3000;

// Define paths for Express config
const publicDirectoryPath = path.join(__dirname, "../public");
const viewsPath = path.join(__dirname, "../templates/views");
const partialsPath = path.join(__dirname, "../templates/partials");

// Define error codes path
const errorCodesPath = path.join(__dirname, "../public/data/errorCodes.json");

let teiFileContent;
let absoluteTeiFilePath;

// Initialization
(() => {
  const teiFileName = process.argv[2];

  if (!teiFileName) {
    console.log(
      "Error: Remember to enter a file name. For example, errald myfile.xml"
    );
    process.exit(1);
  }

  absoluteTeiFilePath = path.resolve(teiFileName);

  fs.stat(absoluteTeiFilePath, (err, stats) => {
    console.log("Errald CLI");
    console.log("==========\n");
    console.log(`Opening: \n  ${absoluteTeiFilePath}`);

    if (err || !stats.isFile()) {
      console.log(
        "Error: This file doesn't exist or there's a problem with it."
      );
      process.exit(1);
    }

    console.log("\nDownloading latest error codes...");

    const doneMsg =
      `\nTo edit this TEI XML file, go to your browser and open:` +
      `\n  localhost:${port}` +
      `\n` +
      `\nUse Ctrl+C to exit.` +
      `\n` +
      `\n---\n`;
    updateErrorCodes(errorCodesPath)
      .then(() => {
        console.log("  Your error codes are up to date.");
        console.log(doneMsg);
      })
      .catch(() => {
        fs.stat(errorCodesPath, (err, stats) => {
          if (err || !stats.isFile()) {
            console.log(
              "  Warning: Couldn't download the latest error codes.\n" +
                "  ---\n" +
                "  Error: You currently don't have any error codes on your computer.\n" +
                "  ---\n" +
                "  Try again when you have internet access.\n\n" +
                "Errald will now close."
            );
            process.exit(1);
          }

          console.log(
            "  Warning: Couldn't download the latest error codes.\n" +
              "  ---\n" +
              "  You might be working with outdated error codes.\n" +
              "  Check your internet connection and restart Errald to download " +
              "the latest error codes."
          );

          console.log(doneMsg);
        });
      });
  });
})();

// Set plain text and JSON parsing
app.use(express.text());
app.use(express.json({ limit: "50mb" }));

// Set handlebars engine and views location
app.set("view engine", "hbs");
app.set("views", viewsPath);
hbs.registerPartials(partialsPath);

// Setup static directory to serve
app.use(express.static(publicDirectoryPath));

// Index route
app.get("/", (req, res) => {
  res.render("index", {
    absoluteTeiFilePath,
  });
});

app.get("/html", (req, res) => {
  teiFileContent = fs.readFileSync(absoluteTeiFilePath, "utf-8");
  const data = toHtml(teiFileContent);
  res.send(data);
});

app.post("/xml", ({ body }, res) => {
  if (!body.html) {
    console.log(
      "Error: Received an empty string, Errald was expecting some HTML.\n" +
        "---\n" +
        "  Errald will not make any changes to the file.\n" +
        "  Please use Ctrl + C to close Errald. Then restart it and " +
        "  refresh your browser."
    );
    res.status(400).send({ error: "Received an empty HTML string." });
    process.exit(1);
  }

  try {
    const outputXml = toXml(body.html, body.teiHeader);

    try {
      const prettyXml = beautifyXml(outputXml);

      fs.writeFile(absoluteTeiFilePath, prettyXml, (err) => {
        if (err) {
          console.log(
            "Error: There was a problem when writing to the file." +
              "---\n" +
              "  Errald will not make any changes to the file.\n" +
              "  Please use Ctrl + C to close Errald. Then restart it and " +
              "  refresh your browser."
          );

          console.log(err);
          res.status(500).send();
          process.exit(1);
        }
        console.log("File saved!");
      });
      res.send();
    } catch (e) {
      console.log(
        "Error: Errald was not able to format the XML before writing the " +
          "changes to the file.\n" +
          "---\n" +
          "  Errald will not make any changes to the file.\n" +
          "  Please use Ctrl + C to close Errald. Then restart it and " +
          "  refresh your browser."
      );
      console.log(e);
      res
        .status(500)
        .send({ error: "Unable to parse HTML and convert it to XML." });
      process.exit(1);
    }
  } catch (e) {
    console.log(
      "Error: Errald has detected a parsing error when converting the latest " +
        "edit to XML.\n" +
        "---\n" +
        "  Errald will not make any changes to the file.\n" +
        "  Please use Ctrl + C to close Errald. Then restart it and " +
        "refresh your browser.\n"
    );
    console.log(e);
    res
      .status(500)
      .send({ error: "Unable to parse HTML and convert it to XML." });
    process.exit(1);
  }
});

app.post("/xml-tokens-to-html", ({ body }, res) => {
  try {
    const outputHtml = xmlTokensToHtml(body.xml);
    res.send({ html: outputHtml, error: "" });
  } catch {
    res.send({
      html: "",
      error: "Please check your input. It doesn't look like valid XML.",
    });
  }
});

app.listen(port, () => {});

/*
TODO:

Update error codes
 - Check if there is an internet connection.
 - If so, then download the TSV file with the error codes.
 - Then generate a new errorCodes.js file for the frontend's error type
   autocompletion.
 - It would be nice to have the UI display the date for the last time the error
   codes where downloaded and updated.
*/
