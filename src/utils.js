const pd = require("pretty-data").pd;

const beautifyXml = (xml) => {
  return pd
    .xml(xml)
    .replace(/TEI \n {2}xmlns/g, "TEI xmlns")
    .replace(/<w> /g, "<w>")
    .replace(/ <\/w>/g, "</w>")
    .replace(/<c> /g, "<c>")
    .replace(/ <\/c>/g, "</c>");
};

module.exports = { beautifyXml };
