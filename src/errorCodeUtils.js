const fs = require("fs");
const parse = require("csv-parse/lib/sync");
const request = require("postman-request");

const url =
  "https://raw.githubusercontent.com/antonkarl/iceErrorCorpus/master/errorCodes.tsv";

const getTsvData = new Promise((resolve, reject) => {
  request(url, (err, res, body) => {
    if (err) {
      reject(Error(`There was a problem with the request.\n${err}`));
    }
    if ((res && res.statusCode) !== 200) {
      reject(
        Error(
          `There was a problem with the request. statusCode: ${
            res && res.statusCode
          }`
        )
      );
    }

    resolve(body);
  });
});

/**
 * Returns the latest errorCodes as an object literal formatted for Materialize
 * CSS's autocomplete feature.
 * @returns {Object} The error codes if successful. Otherwise, an empty object.
 */
const getErrorCodes = async () => {
  try {
    let result = {};
    const data = await getTsvData;

    const parsedData = parse(data, {
      delimiter: "\t",
      columns: true,
      trim: true,
    });

    parsedData.map((record) => {
      result[record.code] = null;

      return null;
    });

    return result;
  } catch (e) {
    return undefined;
  }
};

/**
 * Fetches the most recent error codes and writes them to a JSON file.
 */
const updateErrorCodes = async (path) => {
  const errorCodes = await getErrorCodes();

  if (errorCodes) {
    const data = JSON.stringify(errorCodes);

    fs.writeFile(path, data, (err) => {
      if (err) {
        throw new Error("Could not write downloaded error codes to file.");
      }
    });
  } else {
    throw new Error("Could not download the latest error codes.");
  }
};

module.exports = { updateErrorCodes };
