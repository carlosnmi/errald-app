const cheerio = require("cheerio");

const DEFAULT_XML_TITLE = "";
const DEFAULT_XML_DISTRIBUTOR = "Rannsóknarstofan Mál og tækni";
const DEFAULT_XML_SOURCE_DESC_P = "";

class InvalidInputError extends Error {
  constructor(args) {
    super(args);
    this.name = "InvalidInputError";
  }
}

/**
 * Returns true if the element's next sibling represents a ".tei-w" element.
 * @param {*} element - A Cheerio HTML DOM element object
 * @returns {boolean}
 */
const isNextSiblingAWord = (element) => {
  if (element && element.next) {
    return element.next.attribs.class.includes("tei-w");
  }

  return false;
};

/**
 * Returns true if the bare Cheerio element is a span with the given class name.
 *
 * @example
 *
 * Element as HTML string: <span class="tei-revision" />
 *
 * bareSpanHasClass(bareElement)
 * //=> true
 *
 * @param {*} element - A Cheerio HTML DOM element object
 * @param {String} className - The queried class name
 * @returns {boolean}
 */
const bareSpanHasClass = (element, className) => {
  if (element && className) {
    if (element.type === "tag") {
      if (element.name === "span") {
        const classValuesString = element.attribs.class;
        const classValuesArray = classValuesString.split(" ");

        return classValuesArray.includes(className);
      }
    }
  }

  return false;
};

/**
 * Returns true if the given element is a span with a "tei-revision" class.
 * @param {*} element - A Cheerio HTML DOM element object
 * @returns {boolean}
 */
const isARevision = (element) => {
  return bareSpanHasClass(element, "tei-revision");
};

/**
 * Returns true if the given element is a span with a "tei-w" class.
 * @param {*} element - A Cheerio HTML DOM element object
 * @returns {boolean}
 */
const isAWord = (element) => {
  return bareSpanHasClass(element, "tei-w");
};

/**
 * Returns the "tei-corrected" element inside the given "tei-revision".
 * @param {*} revisionElement - A Cheerio HTML DOM element representing a TEI revision
 * @returns {*} - A Cheerio HTML DOM element or null if undefined
 */
const getCorrectedElement = (revisionElement) => {
  if (revisionElement) {
    if (revisionElement.children.length >= 2) {
      return revisionElement.children[1];
    }
  }

  return null;
};

/**
 * Returns true if the passed element is the last element in the sentence.
 * @param {*} element - A Cheerio HTML DOM element object
 * @returns {boolean}
 */
const isLastElementInSentence = (element) => {
  return element && !element.next;
};

/**
 * Adds given class name to given Cheerio HTML DOM element.
 * @param {*} element - A Cheerio HTML DOM element object
 * @param {*} className - The class name to add
 */
const addClass = (element, className) => {
  element.attribs.class += ` ${className}`;
};

/**
 * Inserts blank space span elements between words and after punctuation marks
 * where appropriate.
 * @param {*} $ - Cheerio instance
 */
const insertBlankSpaces = ($) => {
  const blankSpaceSpan = `<span class="tei-space-character">&#32;</span>`;
  const words = $(".tei-w");
  const punctuationMarks = $(".tei-c");
  const revisions = $(".tei-revision");

  for (let index = 0; index < $(".tei-w").length; index += 1) {
    const currentWord = words.get(index);
    const sibling = currentWord.next;

    if (sibling) {
      if (isAWord(sibling)) {
        addClass(currentWord, "target");
      }

      if (isARevision(sibling)) {
        const correctedElement = getCorrectedElement(sibling);

        if (correctedElement) {
          if (correctedElement.children.length > 0) {
            if (isAWord(correctedElement.children[0])) {
              addClass(currentWord, "target");
            }
          } else {
            const revisionsNextSibling = sibling.next;

            if (revisionsNextSibling) {
              if (isAWord(revisionsNextSibling)) {
                addClass(currentWord, "target");
              }
            }
          }
        }
      }
    }
  }

  if (punctuationMarks.length > 0) {
    /*
    TODO:
    Figure out a way to add a test that justifies checking if the input XML
    or at least the processed HTML so far contains any punctuation mark spans.

    Some tests fail because they call toHtml() with input XML that contains no
    <c> elements. The absence of <c> elements causes the following line to fail:

    $(".tei-c").get(lastTeiCIndex).attribs.class = "tei-c";
    
    That makes sense because we're trying to read a property from an object that
    doesn't exist.
    */
    for (let index = 0; index < punctuationMarks.length; index += 1) {
      if (isNextSiblingAWord(punctuationMarks.get(index))) {
        addClass(punctuationMarks.get(index), "target");
      }
    }

    /*
    TODO:
    Technically, the last punctuation mark in a paragraph should not be
    succeded by a blank space either.
    */
    for (let index = 0; index < punctuationMarks.length; index += 1) {
      if (isLastElementInSentence(punctuationMarks.get(index))) {
        addClass(punctuationMarks.get(index), "target");
      }
    }

    const lastTeiCIndex = punctuationMarks.length - 1;
    punctuationMarks.get(lastTeiCIndex).attribs.class = "tei-c";
  }

  for (let index = 0; index < revisions.length; index += 1) {
    const currentRevision = revisions.get(index);

    if (isAWord(currentRevision.next)) {
      const correctedElement = getCorrectedElement(currentRevision);

      if (correctedElement) {
        if (correctedElement.children) {
          const lastChildIndex = correctedElement.children.length - 1;
          if (isAWord(correctedElement.children[lastChildIndex])) {
            addClass(currentRevision, "target");
          }
        }
      }
    }
  }

  $(blankSpaceSpan).insertAfter(".target");

  $(".target").removeClass("target");
};

/**
 * Returns the XML attribute value encoded in the corresponding HTML element's class.
 * @param {*} element - A Cheerio HTML DOM element object
 * @param {String} classValuePrefix - The class value prefixing the value you
 *   want to extract
 */
const getClassEncodedAttribute = (element, classValuePrefix) => {
  const classValues = element.attribs.class.split(" ");

  const desiredArrayItem = classValues.find((value) =>
    value.includes(classValuePrefix)
  );

  return desiredArrayItem
    ? desiredArrayItem.replace(classValuePrefix, "")
    : null;
};

/**
 * Returns the XML attribute value encoded in the corresponding HTML element's id.
 * @param {*} element - A Cheerio HTML DOM element object
 * @param {String} idValuePrefix - The string prefixing the value you want to
 *   extract
 * @returns {String} The queried value
 */
const getIdEncodedAttribute = (element, idValuePrefix) => {
  if (!element.attribs.id) {
    return null;
  }

  const idValue = element.attribs.id;

  return idValue.includes(idValuePrefix)
    ? idValue.replace(idValuePrefix, "")
    : null;
};

/**
 * Returns the eId value encoded in the HTML span element's class attribute.
 * @param {*} teiErrorElement - DOM element with 'tei-error' class
 */
const getEId = (teiErrorElement) => {
  return getClassEncodedAttribute(teiErrorElement, "tei-error-eid-");
};

/**
 * Returns the error reason value encoded in the HTML span element's class attribute.
 * @param {*} teiErrorElement - DOM element with 'tei-error' class
 */
const getReason = (teiErrorElement) => {
  return getClassEncodedAttribute(teiErrorElement, "tei-error-reason-");
};

/**
 * Returns the xtype value encoded in the HTML span element's class attribute.
 * @param {*} teiErrorElement - DOM element with 'tei-error' class
 */
const getXType = (teiErrorElement) => {
  return getClassEncodedAttribute(teiErrorElement, "tei-error-xtype-");
};

/**
 * Decodes the desired class or id-encoded attribute for all matching elements.
 * @param {String} selector - Cheerio-compatible selector
 * @param {String} attributeName - The name of the attribute you want to decode
 * @param {String} prefix - The string prefixing the attribute
 * @param {String} encodingAttribute - Either "class" or "id". Specifies whether
 *   the desired value is encoded within the HTML elements' "class" or "id"
 *   attribute
 * @param {*} $ - Cheerio instance
 * @returns {Number} Total matching elements found with missing attribute
 */
const decodeAttributeForAllMatchingElements = (
  selector,
  attributeName,
  prefix,
  encodingAttribute,
  $
) => {
  let elementsWithMissingAttribute = 0;

  for (let index = 0; index < $(selector).length; index += 1) {
    const element = $(selector).get(index);

    let attributeValue;
    if (encodingAttribute === "class") {
      attributeValue = getClassEncodedAttribute(element, prefix);
    } else if (encodingAttribute === "id") {
      attributeValue = getIdEncodedAttribute(element, prefix);
    } else {
      throw new Error(
        `${encodingAttribute}' is not a valid encoding attribute. Expected either 'class' or 'id'`
      );
    }

    if (attributeValue) {
      element.attribs[attributeName] = attributeValue;
    } else {
      elementsWithMissingAttribute += 1;
    }
  }

  return elementsWithMissingAttribute;
};

/**
 * Encodes the desired attribute for all matching elements into their class attribute.
 * @param {String} selector - Cheerio-compatible selector
 * @param {String} attributeName - The name of the attribute you want to encode
 * @param {String} classValuePrefix - The class value string you want to prefix to the attribute value
 * @param {*} $ - Cheerio instance
 */
const encodeAttributeIntoClassForAllMatchingElements = (
  selector,
  attribute,
  classValuePrefix,
  $
) => {
  for (let index = 0; index < $(selector).length; index += 1) {
    const originalAttributeValue = $(selector).get(index).attribs[attribute];
    if (originalAttributeValue) {
      const encodedAttributeValue = `${classValuePrefix}${originalAttributeValue}`;

      $(selector).get(index).attribs.class += ` ${encodedAttributeValue}`;
    }
  }
};

/**
 * Encodes the desired attribute for all matching elements into their id attribute.
 * @param {String} selector - Cheerio-compatible selector
 * @param {String} attributeName - The name of the attribute you want to encode
 * @param {String} prefix - The string you want to prefix to the attribute value
 * @param {*} $ - Cheerio instance
 */
const encodeAttributeIntoIdForAllMatchingElements = (
  selector,
  attributeName,
  prefix,
  $
) => {
  for (let index = 0; index < $(selector).length; index += 1) {
    const originalAttributeValue = $(selector).get(index).attribs[
      attributeName
    ];
    const newId = `${prefix}${originalAttributeValue}`;

    $(selector).get(index).attribs.id = newId;
  }
};

/**
 * Sets the tag name attribute for all matched elements.
 * @param {String} selector - Cheerio selector
 * @param {String} tagName - New tag name
 * @param {*} $ - Cheerio instance
 */
const setTagNameForAllMatchedElements = (selector, tagName, $) => {
  for (let index = 0; index < $(selector).length; index += 1) {
    $(selector).get(index).tagName = tagName;
  }
};

/**
 * Returns the serialized HTML tree.
 * @param {*} $ - Cheerio instance
 * @returns {String} The serialized HTML
 */
const getSerializedHtml = ($) => {
  return $("text").html().trim().replace(/\/>/g, "></span>");
};
/**
 * Returns the HTML representation of the TEI XML string.
 * @param {String} inputXml - TEI XML document
 * @returns {Object} The teiHeader info and the serialized HTML
 */
const toHtml = (inputXml) => {
  if (!inputXml) {
    if (inputXml === "") {
      throw new InvalidInputError("Received empty inputXml string.");
    } else {
      throw new InvalidInputError("Missing argument inputXml.");
    }
  }

  const inputXmlWithoutWhitespaceBetweenElements = inputXml
    .trim()
    .replace(/>\s+</g, "><");

  const $ = cheerio.load(inputXmlWithoutWhitespaceBetweenElements, {
    xmlMode: true,
    normalizeWhitespace: true,
  });

  const title = $("title").text();
  const distributor = $("distributor").text();
  const sourceDescP = $("sourceDesc").children().first().text();

  const teiHeader = {
    title: title || "",
    distributor: distributor || "",
    sourceDescP: sourceDescP || "",
  };

  if (!$("w").length) {
    throw new InvalidInputError("Received XML tree contains no word elements.");
  }

  if ($("text").length === 0) {
    throw new InvalidInputError("Received XML tree contains no text element.");
  }

  if ($("text").length > 1) {
    throw new InvalidInputError(
      "Received XML tree contains more than one text element."
    );
  }

  if ($("body").length === 0) {
    throw new InvalidInputError("Received XML tree contains no body element.");
  }

  if ($("body").length > 1) {
    throw new InvalidInputError(
      "Received XML tree contains more than one body element."
    );
  }

  if ($("body").get(0).parentNode.tagName !== "text") {
    throw new InvalidInputError(
      "Body element is not an immediate descendant of the text element."
    );
  }

  if ($(".tei-container").length > 0) {
    throw new InvalidInputError("");
  }

  if ($(".tei-p").length > 0) {
    throw new InvalidInputError("");
  }

  if ($(".tei-s").length > 0) {
    throw new InvalidInputError("");
  }

  if ($(".tei-w").length > 0) {
    throw new InvalidInputError("");
  }

  if ($(".tei-c").length > 0) {
    throw new InvalidInputError("");
  }

  $("body").addClass("tei-container");
  setTagNameForAllMatchedElements("body", "span", $);

  $("p").addClass("tei-p");
  encodeAttributeIntoIdForAllMatchingElements(".tei-p", "n", "tei-p-n-", $);
  $(".tei-p").removeAttr("n");
  setTagNameForAllMatchedElements(".tei-p", "span", $);

  $("s").addClass("tei-s");
  encodeAttributeIntoIdForAllMatchingElements(".tei-s", "n", "tei-s-n-", $);
  $(".tei-s").removeAttr("n");
  setTagNameForAllMatchedElements(".tei-s", "span", $);

  $("w").addClass("tei-w");
  setTagNameForAllMatchedElements(".tei-w", "span", $);

  $("c").addClass("tei-c");
  setTagNameForAllMatchedElements(".tei-c", "span", $);

  $("hi").addClass("tei-hi");
  setTagNameForAllMatchedElements(".tei-hi", "span", $);

  /*
  NOTE:
  <sup> and <sub> elements don't need to be transformed. They are already
  supported in HTML.
  */

  $("original").addClass("tei-original");
  setTagNameForAllMatchedElements(".tei-original", "span", $);

  $("corrected").addClass("tei-corrected");
  setTagNameForAllMatchedElements(".tei-corrected", "span", $);

  $("errors").addClass("tei-errors");
  setTagNameForAllMatchedElements(".tei-errors", "span", $);

  $("error").addClass("tei-error");
  setTagNameForAllMatchedElements(".tei-error", "span", $);
  encodeAttributeIntoClassForAllMatchingElements(
    ".tei-error",
    "xtype",
    "tei-error-xtype-",
    $
  );
  $(".tei-error").removeAttr("xtype");
  encodeAttributeIntoClassForAllMatchingElements(
    ".tei-error",
    "eid",
    "tei-error-eid-",
    $
  );
  $(".tei-error").removeAttr("eid");
  encodeAttributeIntoClassForAllMatchingElements(
    ".tei-error",
    "reason",
    "tei-error-reason-",
    $
  );
  $(".tei-error").removeAttr("reason");
  encodeAttributeIntoClassForAllMatchingElements(
    ".tei-error",
    "depId",
    "tei-error-depId-",
    $
  );
  $(".tei-error").removeAttr("depId");
  encodeAttributeIntoClassForAllMatchingElements(
    ".tei-error",
    "idx",
    "tei-error-idx-",
    $
  );
  $(".tei-error").removeAttr("idx");

  // TODO: I might be able to refactor into a function that takes in
  // the element tag and adds a class of tei-x, where x is the original
  // element tag string.
  $("revision").addClass("tei-revision");
  setTagNameForAllMatchedElements(".tei-revision", "span", $);
  encodeAttributeIntoIdForAllMatchingElements(
    ".tei-revision",
    "id",
    "tei-revision-id-",
    $
  );

  insertBlankSpaces($);

  return {
    teiHeader,
    html: getSerializedHtml($),
  };
};

/*
TODO:
Refactor toHtml() and xmlTokensToHtml() so they comply with DRY. Otherwise, it's
easy to forget to add new XML element handling to both of these functions.
*/

/**
 * Returns a serialized and UI-friendly HTML representation of the given token string.
 * Note that only word and punctuation tokens are accepted.
 * @param {String} inputXml - The tokens you want to convert to HTML
 * @example
 *
 * xmlTokensToHtml("<w>Hi</w><w>there</w><c>.</c>")
 * //=> `<span class="tei-w">Hi</span><span class="tei-space-character">&#32;</span><span class="tei-w">there</span><span class="tei-c">.</span>`
 */
const xmlTokensToHtml = (inputXml) => {
  if (!inputXml) {
    return "";
  }

  const inputXmlWithoutWhitespaceBetweenElements = inputXml
    .trim()
    .replace(/>\s+</g, "><");

  const $ = cheerio.load(inputXmlWithoutWhitespaceBetweenElements, {
    xmlMode: true,
    normalizeWhitespace: true,
  });

  if (!$("w").length && !$("c").length) {
    throw new Error(
      "Received XML tree contains no word or punctuation elements."
    );
  }

  $("w").addClass("tei-w");
  setTagNameForAllMatchedElements(".tei-w", "span", $);

  $("c").addClass("tei-c");
  setTagNameForAllMatchedElements(".tei-c", "span", $);

  $("hi").addClass("tei-hi");
  setTagNameForAllMatchedElements(".tei-hi", "span", $);

  /*
  NOTE:
  <sup> and <sub> elements don't need to be transformed. They are already
  supported in TEI XML.
  */

  insertBlankSpaces($);

  return $.html().trim().replace(/\/>/g, "></span>");
};

/**
 * Processes the given attribute for all matched elements.
 * @param {String} selector - Cheerio-compatible selector
 * @param {String} attributeName - The name of the attribute you want to process
 * @param {*} $ - Cheerio instance
 * @param {*} cb - Function you want to execute on every matched element's
 *   attribute
 */
const processAttributeForAllMatchedElements = (
  selector,
  attributeName,
  $,
  cb
) => {
  const elements = $(selector);

  for (let index = 0; index < elements.length; index += 1) {
    const element = elements.get(index);
    const attributeValue = element.attribs[attributeName];

    element.attribs[attributeName] = cb(attributeValue);
  }
};

/**
 * Returns the serialized XML representation of the given HTML-encoded TEI
 * document.
 * @param {String} inputHtml - The serialized and HTML-encoded TEI document
 * @returns {String} The serialized XML TEI document
 *
 * @example
 *
 * The pretty formatting is just for readability. Both the input and output
 * strings should not contain white space between each element. The only
 * exceptions are the ".tei-space-character" elements which contain a single
 * blank space character as their children.
 *
 * inputHtml:
 *
 *   <span class="tei-p">
 *     <span class="tei-s">
 *       <span class="tei-w">Hello</span>
 *       <span class="tei-space-character"> </span>
 *       <span class="tei-w">dear</span>
 *       <span class="tei-c">.</span>
 *     </span>
 *   </span>
 *
 * const outputXml = toXml(inputHtml);
 *
 * outputXml:
 *
 * TODO: Need to rewrite and incorporate teiHeader contents.
 *
 *   <?xml version="1.0" encoding="utf-8"?>
 *   <TEI xmlns="http://www.tei-c.org/ns/1.0">
 *     <teiHeader></teiHeader>
 *     <text>
 *       <body>
 *         <p n="1">
 *           <s n="1">
 *             <w>Hello</w>
 *             <w>dear</w>
 *             <c>.</c>
 *           </s>
 *         </p>
 *       </body>
 *     </text>
 *   </TEI>
 */
const toXml = (inputHtml, { title, distributor, sourceDescP } = {}) => {
  if (inputHtml.includes("tei-container")) {
    throw new Error(`Unexpected HTML element: ".tei-container"`);
  }

  const processedTitle = title || DEFAULT_XML_TITLE;
  const processedDistributor = distributor || DEFAULT_XML_DISTRIBUTOR;
  const processedSourceDescP = sourceDescP || DEFAULT_XML_SOURCE_DESC_P;

  const xmlDeclaration = `<?xml version="1.0" encoding="utf-8"?>`;
  const bodyElement = `<body>${inputHtml}</body>`;
  const textElement = `<text>${bodyElement}</text>`;
  const teiHeaderElement =
    `<teiHeader>` +
    `<fileDesc>` +
    `<titleStmt><title>${processedTitle}</title></titleStmt>` +
    `<publicationStmt><distributor>${processedDistributor}</distributor></publicationStmt>` +
    `<sourceDesc><p>${processedSourceDescP}</p></sourceDesc>` +
    `</fileDesc>` +
    `</teiHeader>`;
  const teiElement = `<TEI xmlns="http://www.tei-c.org/ns/1.0">${teiHeaderElement}${textElement}</TEI>`;
  const baseXmlTree = `${xmlDeclaration}${teiElement}`;

  const $ = cheerio.load(baseXmlTree, { xmlMode: true, decodeEntities: false });

  setTagNameForAllMatchedElements(".tei-p", "p", $);
  decodeAttributeForAllMatchingElements(".tei-p", "n", "tei-p-n-", "id", $);
  $(".tei-p").removeAttr("id");
  $(".tei-p").removeAttr("style");
  $(".tei-p").removeAttr("class");

  setTagNameForAllMatchedElements(".tei-s", "s", $);
  decodeAttributeForAllMatchingElements(".tei-s", "n", "tei-s-n-", "id", $);
  $(".tei-s").removeAttr("id");
  $(".tei-s").removeAttr("style");
  $(".tei-s").removeAttr("class");

  setTagNameForAllMatchedElements(".tei-w", "w", $);
  $(".tei-w").removeAttr("style");
  $(".tei-w").removeAttr("class");

  setTagNameForAllMatchedElements(".tei-c", "c", $);
  $(".tei-c").removeAttr("style");
  $(".tei-c").removeAttr("class");

  setTagNameForAllMatchedElements(".tei-hi", "hi", $);
  $(".tei-hi").removeAttr("style");
  $(".tei-hi").removeAttr("class");

  /*
  NOTE:
  <sup> and <sub> elements don't need to be transformed. They are already
  supported in TEI XML.
  */

  setTagNameForAllMatchedElements(".tei-errors", "errors", $);
  $(".tei-errors").removeAttr("class");

  processAttributeForAllMatchedElements(".tei-revision", "id", $, (value) =>
    value.replace("tei-revision-id-", "")
  );
  setTagNameForAllMatchedElements(".tei-revision", "revision", $);
  $(".tei-revision").removeAttr("style");
  $(".tei-revision").removeAttr("class");

  setTagNameForAllMatchedElements(".tei-original", "original", $);
  $(".tei-original").removeAttr("class");

  setTagNameForAllMatchedElements(".tei-corrected", "corrected", $);
  $(".tei-corrected").removeAttr("class");

  setTagNameForAllMatchedElements(".tei-error", "error", $);
  const errorElementsWithMissingXtype = decodeAttributeForAllMatchingElements(
    ".tei-error",
    "xtype",
    "tei-error-xtype-",
    "class",
    $
  );
  if (errorElementsWithMissingXtype) {
    throw new Error("Found '.tei-error' element with a missing xtype");
  }
  decodeAttributeForAllMatchingElements(
    ".tei-error",
    "reason",
    "tei-error-reason-",
    "class",
    $
  );
  decodeAttributeForAllMatchingElements(
    ".tei-error",
    "depId",
    "tei-error-depId-",
    "class",
    $
  );
  decodeAttributeForAllMatchingElements(
    ".tei-error",
    "eid",
    "tei-error-eid-",
    "class",
    $
  );
  decodeAttributeForAllMatchingElements(
    ".tei-error",
    "idx",
    "tei-error-idx-",
    "class",
    $
  );
  $(".tei-error").removeAttr("class");

  $(".tei-space-character").remove();

  return $.html();

  /*
  TODO:
  Throw an exception if there's still span elements in the resulting XML tree.
  This would mean that the HTML tree contained elements that toHtml() hasn't
  been designed to transform.
  */
};

// TODO: Remove getEId(), getReason(), and getXType()
module.exports = {
  getClassEncodedAttribute,
  getIdEncodedAttribute,
  getEId,
  getReason,
  getXType,
  toHtml,
  toXml,
  xmlTokensToHtml,
};
