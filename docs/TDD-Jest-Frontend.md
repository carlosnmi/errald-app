# HOW TO USE JEST TO UNIT TEST FRONTEND JAVASCRIPT

## Use JSDOM to get a simulated DOM inside a Node.js/Jest environment

You can write tests for code that manipulates the browser's DOM by using JSDOM. JSDOM is a plain JavaScript simulation of the browser's DOM. Just beware that there are a few features not implemented by JSDOM, like HTML elements' `innerText` property.

More info on JSDOM at:

- https://www.testim.io/blog/jsdom-a-guide-to-how-to-get-started-and-what-you-can-do/
- https://github.com/jsdom/jsdom

Info on JSDOM and `innerHTML`:

- https://github.com/jsdom/jsdom/issues/1245

## You may have to transpile your code using a tool like Babel

Jest can be used to write unit tests for both backend and frontend JavaScript. But Jest v26.1.0 will complain when you import frontend JavaScript modules that use ES6's import/export statements. The error usually looks like this:

`SyntaxError: Unexpected token 'export'`

For example, here's the code for an ES6 module called `math.js`. It defines a function called `add()`:

```
// In math.js

const add = (a, b) => {
    return a + b;
};

export { add };
```

Here's how the frontend's `app.js` file would import the `math` module:

```
// In app.js

import { add } from "./math.js";

// more code ...

const result = add(2, 3);

// do something with the result...
```

Now here's a file called `math.test.js`. It contains a unit test for `add()`:

```
// In math.test.js

const { add } = require("../public/js/math");
const { TestScheduler } = require("jest");

describe("add()", () => {
  test("Returns expected value.", () => {
    const result = add(2, 3);

    expect(result).toBe(5);
  });
});

```

Here's the project's structure. Files and directories irrelevant to this example have been omitted:

```
myproject/
|-- public/
|   |-- js/
|       |-- app.js
|       |-- math.js
|-- src/
|   |-- app.js
|   |-- utils.js
|-- tests/
    |-- math.test.js
    |-- utils.test.js
```

The project represents a fullstack JavaScript project. The `public` directory contains all the frontend code. `public/js/app.js` is the file that handles most of the frontend's scripting and `math.js` contains some math functions that get imported by `app.js` using ES6's `import` syntax.

The`src`directory contains the backend code. For example,`src/app.js` is where Node.js/Express handles all the HTTP routing. And`utils.js`is a file that get's imported by`app.js`using Node's`require()` syntax.

If you run `jest` through the CLI or using `npm run test` you will get an error about the `export` statement in `math.js`. After reading online, I found that Jest only has experimental support for ES6 modules (according to the CommonJS syntax). There are a few workarounds to solve this problem. One is to enable experimental support. But the one that I tried first and worked for me was to use Babel to transform your ES2015 (ES6) modules. You can do this by installing Babel's ES2015 transform plugin:

```
$ npm i --save-dev @babel/plugin-transform-modules-commonjs
```

Then tell Jest to use Babel's plugin by adding a `babel` configuration key to your `package.json` file:

```
{
  ...
  "name": "myproject",
  "version": "1.0.0",
  "babel": {
    "env": {
      "test": {
        "plugins": [
          "@babel/plugin-transform-modules-commonjs"
        ]
      }
    }
  },
  "scripts": {
    "start": "node src/app.js",
    "dev": "nodemon src/app.js -e js,hbs",
    "test": "jest --watch"
  },
  ...
}
```

> Check this StackOverflow post for additional info on transpiling when using Jest: https://stackoverflow.com/questions/35756479/does-jest-support-es6-import-export
