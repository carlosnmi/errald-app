# TEI Parser Specification

## Base TEI tree used for all the examples

## Basic specifications

- It only needs to inspect the `s` elements in sequential order.

- The following elements can be ignored:
  - `xml`
  - `TEI`
  - `teiHeader`
  - `text`
  - `body`
  - `p`

### Transformations

#### Sentence elements

`s` (sentence) elements should be converted to `span` elements. For example:

Input:

```
<s n="1"></s>
```

Output:

```
<span class="tei-s"></span>
```

So the `id` attribute is set to `tei-s-x` where `x` corresponds to the value of the original `s` element's `n` attribute.

#### Word elements

`w` (word) elements should be converted to span elements. For example:

Input:

```
<w>Hi</w>
```

Output:

```
<span class="tei-w">Hi</span>
```

#### Punctuation elements

`c` (punctuation) elements can contain periods, commas, exclamation marks, etc. They should be converted to `span` elements that look like this:

Input:

```
<c>.</c>
```

Output:

```
<span class="tei-c">.</span>
```

#### Insertions

The TEI parser also needs to insert white spaces between words and puctuation marks to make the rendered HTML human-readable. Single blank space characters could be encoded as `span` elements. For example:

```
<span class="tei-blank-space"> </span>
```

Here the `span`'s inner HTML is set to an actual blank space character (ASCII 32).

Now, let's imagine the input TEI XML file contains these two sentences:

```
Hi, my name is Carlos. How are you?
```

This means the original XML file contains the following element tree:

```
<s n="1">
  <w>Hi</w><c>,</c><w>my</w><w>name</w><w>is</w><w>Carlos</w><c>.</c>
  <w>How</w><w>are</w><w>you</w><c>?</c>
</s>
```

To render the sentences in human-readable form on the UI, the transformation function needs to generate this HTML tree:

```
<span class="tei-s">
  <span class="tei-w">Hi</span>
  <span class="tei-c">,</span>
  <span class="tei-blank-space"> </span>
  <span class="tei-w">my</span>
  <span class="tei-blank-space"> </span>
  <span class="tei-w">name</span>
  <span class="tei-blank-space"> </span>
  <span class="tei-w">is</span>
  <span class="tei-blank-space"> </span>
  <span class="tei-w">Carlos</span>
  <span class="tei-c">.</span>
  <span class="tei-blank-space"> </span>
  <span class="tei-w">How</span>
  <span class="tei-blank-space"> </span>
  <span class="tei-w">are</span>
  <span class="tei-blank-space"> </span>
  <span class="tei-w">you</span>
  <span class="tei-c">?</span>
</span>
<span class="tei-s">
</span>
```

## Rules for inserting spaces after punctuation marks

**1** Insert a blank space if the punctuation mark is one of the following:

- Comma
- Colon
- Semicolon

But only if the punctuation mark is not the last token in the sentence.

**2** Insert a blank space after the punctuation mark if the punctuation mark is one of the following:

- Period
- Question mark
- Exclamation mark

But only if the sentence is followed by another sentence. In other words, this rule doesn't apply for the last sentence in an XML file.
