# STATES, ACTIONS AND TRANSITIONS

## idle

- Load token

## displayingWord

- Load token
- Select multiple tokens
- Convert word to revision

## convertingWordToRevision

- Cancel and go back to `displayingWord`
- Submit new revision

> Errors in submitted data will just cause a `submissionError` message to be updated in the Store. The tokenInfo component will just render the message if it's not an empty string.

## displayingPunctuation

- Load token
- Select multiple tokens
- Convert punctuation to revision

## convertingPunctuationToRevision

- Cancel and go back to `displayingPunctuation`
- Submit new revision

## displayingRevision

- Load token
- Update corrected text
- Append error to revision
- Update error in revision
- Remove error from revision

## appendingErrorToRevision

- Cancel and go back to `displayingRevision`
- Submit error

## selectingMultipleTokens

- Cancel and go back to displaying the previously selected token
- Confirm selection

## convertingSelectionToRevision

- Cancel and go back to displaying the previously selected token
- Submit new revision
