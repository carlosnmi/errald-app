inputXml = `<p n="1"><s n="1"><w>Hello</w><w>world</w></s><s n="2"><w>Bye</w><w>for</w><w>now</w></s></p>`

=========

```
let inputXml

inputXml = `<?xml version="1.0" encoding="utf-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0"><teiHeader></teiHeader><text><body><p n="1"><s n="1"><w>Hello</w><w>my</w><w>darling</w></s><s n="2"><w>I</w><w>love</w><w>you</w><w>very</w><w>much</w></s></p></body></text></TEI>`

cheerio = require("cheerio")

let $

$ = cheerio.load(inputXml, {xmlMode: true})
```

## Cheerio's objects

Methods like `$("p")` return a Cheerio collection.
Methods like `$("p").get(0)` return a DOM node object.

For example `$("p").tagName` is undefined because the collection object returned by `$("p")` doesn't contain a `tagName` property. In contrast, `firstP = $("p").get(0)` will return the first "p" DOM node object in the `$("p")` collection. And `firstP` does have a `tagName` property. So `firstP.tagName` returns `"p"`.

To get the children for the first sentence one can use

`firstSentencesChildren = $("s").children()`

Here, `children()` is only going to return the children for the first sentence in the `$("s")` collection.

## Manipulating the class attribute

To add a class to the first object in a collection:

`$(selector).attr(name, value)`

To remove a class from the first object in a collection:

`$(selector).attr(name, null)`

or

`$(selector).removeAttr(name)`

To add a class to all of the matched elements (useful for processing, `p` and `s` elements).

`$(selector).addClass(className);`

## DOM node object properties

DOM node objects define these properties:

- tagName
- parentNode
  - Returns a DOM node object
- previousSibling
  - Returns a DOM node object
- nextSibling
  - Returns a DOM node object
- nodeValue
- firstChild
  - Returns a DOM node object
- childNodes
- lastChild
  - Returns a DOM node object

## How do I modify all the tagName properties for all the `p` and `s` elements?

let index = 0;

for (index = 0; index < $("p").length; index++) {
  $("p").get(0).tagName = "span"
}

## Quickest way to transfor TEI XML to UI friendly HTML

Load the input TEI XML string into Cheerio.

### Fail fast

If the document contains no words then signal an error.

```
if ($("w").length == 0) {
  // Stop processing and signal error
}
```

### Check if there is a single text element

The input XML string must contain a `text` element that encloses the `body` element. To get the HTML transformed `body` element and its descendants, I'll need to call:

```
$("text").html()
```

### Process the body tag

Check if the resulting XML tree has a `body` element. There should only be a single `body` element.

```
if ($("body").length == 1) {
  // Process the document
}
else {
 // Don't process the document. It's invalid.
}
```

If there is a single `body` element then add a class of `tei-container` to the `body` element. Then modify the `body`'s `tagName` to `span`.

The
`$("body").attr("class", "tei-container");`

#### Testing

There is only one element with the class of `tei-container`.
The `tei-container` element is at the root of the HTML tree.

### Process the p tags

Check if the tree has any paragraph elements. If it does, continue to process the document. Otherwise stop processing and return or signal an error.

Add a class of `tei-p` to all paragraph elements. Then modify all the `p` tags to `span`.

### Process the s tags

Check if the tree has any sentence elements.

### Append blank space spans where appropriate

### Process the w tags

If not enclosed within a revision:

Add a class of `tei-w` to the `w` elements. Then modify their tags to `span`.

If enclosed within a `revision` element:

There's two possibilities:

If enclosed within an `original` element.
Add a class of `tei-w-original`.

Or if enclosed within a `corrected` element, add a class of `tei-w-corrected`.

There's an except

### Process the c tags

Same rules as for the `w` elements but use `tei-c-original` and `tei-c-corrected` instead.

### Process the errors and error tags

Remove all `errors` tags for now.

===========================

## Problems

### children() bug

`children()` gets applied to all descendants, not just immediate descendants. This is a known bug and hasn't been solved yet.

https://github.com/cheeriojs/cheerio/issues/17

Example XML:

`<p n="1"><s n="1"><w>Hello</w><w>world</w></s><s n="2"><w>Bye</w><w>for</w><w>now</w></s></p>`

The following call should return 2, but instead it returns 6. The first sentence only contains two children, the word tags for `Hello` and `world`.

`childrenInFirstSentence = $("s").children().length //=> 6`

Also, if you iterate over the collection returned by `.children()` you do get the words `Bye`, `for`, and `now` instead of just `Hello` and `world`.

One workaround is to query the actual length of the DOM node object's children array.

For example:

If I want to get the number of children in the first sentence, I can run:

`$("s").get(0).children.length //=>2`

This also works for the second sentence:

`$("s").get(1).children.length //=>3`

### siblings() bug

Example XML:

`<theroot><child1/><child2/></theroot>`

Asking for `child1`'s number of siblings gives the expected result:

`$("child1").siblings().length //=> 1`

However, this only works if `child1` and `child2` have the same parent element, that's `theroot` in this case.

Take this other XML string and try asking for the number of `child1`'s siblings:

`<child1/><child2/>`
`$("child1").siblings().length //=> 0`

The result is zero because Cheerio seems to require `.siblings()` to only return items with the same immediate parent.

The funny thing is that asking for `child1`'s `nextSibling` will still give a reference to `child2`. Try it yourself:

`$("child1").get(0).nextSibling.tagName //=> "child2`

And asking for `child2`'s `previousSibling` will give a reference to `child1`:

`$("child2").get(0).previousSibling.tagName`

### Inserting elements between any arbitrary elements.

Example XML:

``<p n="1"><s n="1"><w>Hello</w><w>world</w></s><s n="2"><w>Bye</w><w>for</w><w>now</w></s></p>`

Suppose I want to modify the first sentence so it reads "Hello dear world". I'd have to insert `<w>dear</w>` between `<w>Hello</w>` and `<w>world</w>`.

Cheerio doesn't have a native way to do this. I'd somehow have to create a new DOM object and use the `.nextSibling` and `.previousSibling` properties to insert `<w>dear</w>` where I want it.

One workaround I found was to take advantage of Cheerior's `.insertAfter(target)` method.

Using the following approach, I was able to insert `<w>dear</w>` between `<w>Hello</w>` and `<w>world</w>`:

```
// Get the collection of all `w` elements and add a class of `targetNode` to the `<w>Hello</w>` element.

$("w").get(0).attribs.class = "targetNode"

// Use `.insertAfter(target)` to insert `<w>dear</w>` after `<w>Hello</w>`
$(`<w>dear</w>`).insertAfter(".targetNode")

// Remove the `targetNode` class from the target node.
$(".targetNode").removeAttr("class")
```
