# HTML Entities for Icelandic Charcters

Á - &#193; or &#xC1; A with acute
Æ - &#198; or &#xC6; AE
É - &#201; or &#xC9;
Í - &#205; or &#xCD;
Ð - &#208; or &#xD0; eth
Ó - &#211; or &#xD3;
Ú - &#218; or &#xDA;
Ö - &#214; or &#xD6; O with diaeresis
Ý - &#221; or &#xDD;
Þ - &#222; or &#xDE; thorn
á - &#225; or &#xE1;
æ - &#230; or &#xE6;
é - &#233; or &#xE9;
í - &#237; or &#xED;
ð - &#240; or &#xF0;
ó - &#243; or &#xF3;
ö - &#246; or &#xF6;
ú - &#250; or &#xFA;
ý - &#253; or &#xFD;
þ - &#254; or &#xFE;

Then there's the punctuation marks like the icelandic quotation marks

“ - &#8220; or &#201C; left double quotation mark (closing)
„ - &#8222; or &#201E; double low-9 quotation mark (opening)
